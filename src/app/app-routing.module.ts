import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './pages/error/error.component';
import { PagesComponent } from './pages/pages.component';
import { AppGuardService } from './services/app-guard.service';

const routes: Routes = [
  /* { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'cadastro', loadChildren: () => import('./pages/cadastro/cadastro.module').then(m => m.CadastroModule) },
  { path: 'atualizar-senha', loadChildren: () => import('./pages/atualizarsenha/atualizarsenha.module').then(m => m.AtualizarsenhaModule) },
  { path: 'recuperar-senha', loadChildren: () => import('./pages/recuperarsenha/recuperarsenha.module').then(m => m.RecuperarsenhaModule) }, */
  { path: '', component: PagesComponent, canActivate: [AppGuardService], loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule) },
  { path: '404', component: ErrorComponent, loadChildren: () => import('./pages/error/error.module').then(m => m.ErrorModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
