import { LoginService } from 'src/app/services/login.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { ApiService } from './services/api.service';
import { LoadingService } from './services/loading.service';
import { Router } from '@angular/router';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'escrituras';

  get loading() {
    return this.loadingService.isActive
  }

  constructor(

    public loadingService: LoadingService,
    private cdr: ChangeDetectorRef,
    public apiService: ApiService,
    public loginService: LoginService,
    public router: Router
  ) { }

  ngOnInit(): void {
 /*    let token = localStorage.getItem('token')
    if (token && window.location.pathname != '/login') {
      this.loginService.getCartorio()
    } else {
      let t = window.location.pathname.search('esqueceu-senha')
      if (t < 0) {
        this.router.navigate(['/login'])
      }
    } */

   /*  let i = window.location.pathname.lastIndexOf('/')
    let url = window.location.pathname.substr(i)
    let token = url.replace("/", "");
    this.apiService.getApiToken(`/sys/token`, token).subscribe((res: any) => {
      localStorage.setItem('token', res.token)
    }) */
  }

  ngAfterViewChecked() {
    this.cdr.detectChanges();
  }
}
