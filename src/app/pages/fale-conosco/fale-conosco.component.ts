import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { FaleConosco } from 'src/app/models/faleConosco';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/toast.service';
import { isDebuggerStatement } from 'typescript';
import validator from 'validar-telefone';
import { LoadingService } from './../../services/loading.service';

interface Types {
  chaveAssunto: string;
  descricaoAssunto: string;
  grupoAssunto: string;
}

@Component({
  selector: 'app-fale-conosco',
  templateUrl: './fale-conosco.component.html',
  styleUrls: ['./fale-conosco.component.scss']
})

export class FaleConoscoComponent implements OnInit {
  @Input() files = [];
  @Input() filesShow = [];

  public tipoAssunto = '';
  public form: FormGroup;
  public types: Array<Types> = [];
  public codigoCartorio = '';
  public error = {
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }

  constructor(private fb: FormBuilder,
    private loadingService: LoadingService,
    private toast: ToastService,
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute) {
    this.codigoCartorio = this.route.snapshot.params['id'];
    this.tipoAssunto = this.route.snapshot.queryParams['tipoSolicitacao']
    this.form = this.fb.group({
      ...FaleConosco.new(this.tipoAssunto)
    })
    this.api.get(`/dominio/listar-assuntos-faleconosco`).subscribe((res: any) => {
      this.types = res
      this.types.sort((a, b) =>
      String(a.descricaoAssunto).localeCompare(String(b.descricaoAssunto))
    );
    })
  }

  ngOnInit(): void {
  }

  phoneNumberIsReal(data: string) {
    if (data && data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        this.form.controls.telefone.setErrors({ 'invalid': true })
        this.form.controls.telefone.markAsTouched()
      } else {
        this.error.phone.message = ''
      }
    }
  }

  validacaoEmail(field: string) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.form.controls.email.setErrors(null)
      } else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.form.controls.email.setErrors({ 'invalid': true })
      }
    }
  }

  atualizarValor(codigo: string) {
    this.form.get('arquivo')?.setValue(codigo);

  }

  submit() {
    if (this.form.valid) {
      this.api.postApi(`/faleconosco/salvar`, this.form.value).subscribe((res: any) => {
        this.toast.throwSnack('Mensagem enviada com sucesso!')
        this.form.reset()
        this.files = []
        this.filesShow = []
      })
    }else if(this.form.controls.assunto.value == "RECLAMACAO" || this.form.controls.assunto.value =="DENUNCIA" || this.form.controls.assunto.value =="OUVIDORIA"){
      if(this.form.controls.msg.invalid){
        this.form.controls.msg.markAllAsTouched()
        this.toast.throwSnack('O campo em vermelho esta inválido.')
      }else{
        this.api.postApi(`/faleconosco/salvar`, this.form.value).subscribe((res: any) => {
          this.toast.throwSnack('Mensagem enviada com sucesso!')
          this.form.reset()
          this.error.phone.message = ''
          this.error.email.message = ''
          this.files = []
          this.filesShow = []
        })
      }
    }else {
      let controls = this.form.controls
      for (let c in controls) {
        if (controls[c].invalid) {
          controls[c].markAllAsTouched()
        }
      }
      this.toast.throwSnack('Os campos em vermelho estão inválidos.')
    }
  }
}
