import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FaleConoscoComponent } from './fale-conosco.component';
import { FaleConoscoRoutingModule } from './fale-conosco-routing.module';
import { DocumentosComponent } from './documentos/documentos.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [FaleConoscoComponent, DocumentosComponent],
  imports: [
    CommonModule,
    FaleConoscoRoutingModule,
    NgxMaskModule,
    ReactiveFormsModule,
    MatIconModule,
  ]
})
export class FaleConoscoModule { }
