import { FinishComponent } from '../../fale-conosco/finish/finish.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { FinishRoutingModule } from '../../fale-conosco/finish/finish-routing.module';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [FinishComponent],
  imports: [
    CommonModule,
    FinishRoutingModule,
    MatIconModule,
    MatCardModule,
  ]
})
export class FinishModule { }
