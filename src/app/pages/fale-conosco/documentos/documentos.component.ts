import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss']
})
export class DocumentosComponent implements OnInit {

  @Output() emitirCodigo = new EventEmitter();

  @Input() files: Array<any> = [];
  @Input() filesShow: Array<any> = [];

  constructor(private loadingService: LoadingService,
    private apiService: ApiService,
    private toast: ToastService) { }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.files.currentValue === []) {
      this.files = []
      this.filesShow = []
    }
  }

  handleSelect(event: any) {
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        this.toast.throwSnack(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`);
      }
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.post('/bucket/upload', formData).subscribe((res: any) => {
      this.loadingService.isActive = false;
      this.files.push(res.codigoArquivo);
      this.filesShow.push({ nome: fileList.name })
      this.emitirCodigo.emit(this.files);
    }, err => {
    })
  }

  deleteFiles(index: any) {
    this.files.splice(index, 1)
    this.filesShow.splice(index, 1)

  }
}
