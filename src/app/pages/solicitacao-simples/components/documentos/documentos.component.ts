import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FileType } from 'src/app/models/uploads';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss']
})
export class DocumentosComponent implements OnInit {
  @Output() documents = new EventEmitter();

  public arquivo: Array<string> = [];
  public files: Array<FileType> = [];

  constructor(private loadingService: LoadingService, private apiService: ApiService) { }

  ngOnInit(): void {
  }

  async handleSelect(event: any) {
    let array = Array.from(event.target.files)
    await array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        alert(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`) //mudar pra toast
      }
    })
    event.target.value = ''
  }



  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.post('/bucket/upload', formData).subscribe((res: any) => {
      this.files.push({ nome: fileList.name })
      this.arquivo.push(res.codigoArquivo)
      this.documents.emit(this.arquivo)
    })
  }


    deleteFile(index: any) {
      this.files.splice(index, 1)
      this.arquivo.splice(index, 1)

    }

}
