import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DocumentosComponent } from '../documentos/documentos.component';
import { NovaPendenciaComponent } from './nova-pendencia.component';


@NgModule({
  declarations: [NovaPendenciaComponent, DocumentosComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    NovaPendenciaComponent
  ]
})
export class NovaPendenciaModule { }
