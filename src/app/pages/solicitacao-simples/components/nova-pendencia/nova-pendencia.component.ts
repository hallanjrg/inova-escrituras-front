import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-nova-pendencia',
  templateUrl: './nova-pendencia.component.html',
  styleUrls: ['./nova-pendencia.component.scss']
})
export class NovaPendenciaComponent implements OnInit {

  @Input() pendencia: any
  public mensagem: string = ''
  public arquivo: Array<string> = []

  constructor(public activeModal: NgbActiveModal,
    private api: ApiService,
    private toast: ToastService,
    ) { }

  ngOnInit(): void {

  }

  sendPendingMessage() {
        const message = {
          arquivo: this.arquivo,
          codigoSolicitacao: this.pendencia.codigoSolicitacao,
          codigoPendencia: this.pendencia.codigo,
          mensagem: this.mensagem
        }
        this.api.postApi(`/chat/enviar`, message).subscribe(res => {
          this.solve();
        })
      }


  solve() {
    this.pendencia.resolvido = null
    this.api.postApi(`/pendencia/save`, this.pendencia).subscribe(res => {
      this.activeModal.close(res)
    })
  }

  setDocument(e: any) {
    this.arquivo = e;
  }


}
