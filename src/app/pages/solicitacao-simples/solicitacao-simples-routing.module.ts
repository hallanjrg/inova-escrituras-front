import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitacaoSimplesComponent } from './solicitacao-simples.component';

const routes: Routes = [
  { path: '', component: SolicitacaoSimplesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitacaoSimplesRoutingModule { }
