import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { SolicitacaoSimplesRoutingModule } from './solicitacao-simples-routing.module';
import { SolicitacaoSimplesComponent } from './solicitacao-simples.component';


const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [SolicitacaoSimplesComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SolicitacaoSimplesRoutingModule,
    NgxMaskModule.forRoot(maskConfig),
    ReactiveFormsModule,
    MatIconModule,
  ]
})
export class SolicitacaoSimplesModule { }
