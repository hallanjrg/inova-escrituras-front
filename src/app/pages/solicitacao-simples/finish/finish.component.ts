import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { ApiService } from 'src/app/services/api.service';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {

  data: any = null
  tipoEscritura: any
  codigoCartorio: any
  codigo: any
  date = moment(new Date()).format('DD/MM/YYYY')

  constructor(private route: Router,
    private activatedRoute: ActivatedRoute,
    public infoService: InfoService,
    public loadingService: LoadingService,
    private api: ApiService) { }

  ngOnInit(): void {
    this.codigo = this.activatedRoute.snapshot.queryParams['codigoSimples']
    this.codigoCartorio = this.activatedRoute.snapshot.queryParams['codigoCartorio']
    this.tipoEscritura = this.activatedRoute.snapshot.queryParams['tipoSolicitacao']
    this.api.getApi(`/solicitacao/get?codigo=${this.codigo}`).subscribe(res => {
      this.data = res
    })
  }

  sendHome() {
    this.route.navigate([`${this.data.tipoEscritura}/${this.codigoCartorio}`],
      {
        queryParams: {
          codigoCartorio: this.codigoCartorio,
          codigoSimples: this.data.codigo,
          tipoEscritura: this.data.tipoEscritura
        }
      }
    )
  }

  redirect() {
    this.route.navigate([`/solicitacao-simples/consulta/${this.activatedRoute.snapshot.params.codigoCartorio}`])
  }

}
