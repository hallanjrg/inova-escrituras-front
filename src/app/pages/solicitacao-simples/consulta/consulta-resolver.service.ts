import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Solicitation } from 'src/app/models/solicitacaoSimples';
import { ApiService } from 'src/app/services/api.service';
import { Observable, forkJoin, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ConsultaResolverService implements Resolve<any> {
  public tokenVazio: any;
  public err: boolean = false

  constructor(private api: ApiService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, rstate: RouterStateSnapshot) {

    const { codigo, email } = route.queryParams
    let i = rstate.url.lastIndexOf('?')
    const url = rstate.url.substring(i, 0)
    let codigoId = window.location.pathname.lastIndexOf('/')
    let urlCodigoId = window.location.pathname.substr(codigoId)
    let token = urlCodigoId.replace("/", "");

    if(token == ""){
      this.tokenVazio = url
      this.tokenVazio = this.tokenVazio.replace('/solicitacao-simples/consulta/', '')
    }
    return this.api.getApiToken('/sys/token', token ? token : this.tokenVazio).pipe(mergeMap((r: any )=> {
      localStorage.setItem('tkn_esc_ext', r.token);
      if (codigo && email) return this.api.getApi(`/solicitacao/obter?codigoSimples=${codigo}&email=${email}`).pipe(
        map(r => {
          this.err = false
          const ret = Solicitation.new(r)
          let pendencias = this.api.getApi(`/pendencia/listar?codigoSolicitacao=${ret.infoSolicitation.codigoInterno}`);
          let chat = this.api.getApi(`/chat/listar?codigoSolicitacao=${ret.infoSolicitation.codigoInterno}`);
          let arquivos = this.api.getApi(`/solicitacao/listarregistros?codigo-escritura=${ret.infoSolicitation.codigo}`)
          let t: any = { ...ret, aditional: this.api.postForkjoin(pendencias, chat, arquivos) }
          return t
        }),
        catchError((error) => {
          this.err = true
          throw this.router.navigate([url], {
            queryParams: {
              error: true,
            }
          })
        })
      )

      else return of(null);
    }))


  }

}
