import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsultaRoutingModule } from './consulta-routing.module';
import { ConsultaComponent } from './consulta.component';
import { ResultsComponent } from './results/results.component';
import { ConcernedsComponent } from './results/concerneds/concerneds.component';
import { SolicitorDataComponent } from './results/solicitor-data/solicitor-data.component';
import { PendenciasComponent } from './results/pendencias/pendencias.component';
import { NovaPendenciaModule } from '../components/nova-pendencia/nova-pendencia.module';
import { HistoryComponent } from './results/pendencias/history/history.component';
import { ChatComponent } from './results/chat/chat.component';
import { NgxMaskModule } from 'ngx-mask';
import { Download } from 'ng-bootstrap-icons/icons';
import { BootstrapIconsModule } from 'ng-bootstrap-icons';
import { DocumentosComponent } from './results/documentos/documentos.component';
import { ConfirmValidacaoComponent } from './results/documentos/confirm-validacao/confirm-validacao.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

const icons = {
  Download
};

@NgModule({
  declarations: [ConsultaComponent, ResultsComponent, ConcernedsComponent, SolicitorDataComponent, PendenciasComponent, HistoryComponent, ChatComponent, DocumentosComponent, ConfirmValidacaoComponent],
  imports: [
    CommonModule,
    ConsultaRoutingModule,
    ReactiveFormsModule,
    NovaPendenciaModule,
    NgbTooltipModule,
    NgxMaskModule,
    FormsModule,
    BootstrapIconsModule.pick(icons),

  ]
})
export class ConsultaModule { }
