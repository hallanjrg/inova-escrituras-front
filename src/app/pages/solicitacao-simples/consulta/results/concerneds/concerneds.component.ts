import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-concerneds',
  templateUrl: './concerneds.component.html',
  styleUrls: ['./concerneds.component.scss']
})
export class ConcernedsComponent implements OnInit {
  public showContainer: any;
  @Input() concerneds: any
  @Input() bens: any
  @Input() compradores: any
  @Input() vendedores: any
  @Input() doadores: any
  @Input() beneficiarios: any

  constructor(public breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.breakpointObserver
    .observe(['(max-width: 750px)'])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.showContainer = true;
      } else {
        this.showContainer = false;
      }
    });
  }

}
