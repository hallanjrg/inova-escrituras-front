import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ESCRITURA_COMPRA_VENDA, ESCRITURA_DOACAO } from 'src/app/models/translation';
import { ConsultaResolverService } from '../../consulta-resolver.service';
import { ConfirmValidacaoComponent } from './confirm-validacao/confirm-validacao.component';

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styleUrls: ['./documentos.component.scss']
})
export class DocumentosComponent implements OnInit {
  public showContainer: any;
  @Input() arquivos: any
  @Input() infoSolicitation: any
  @Output() refresh = new EventEmitter()

  public translations: any = ESCRITURA_COMPRA_VENDA

  constructor(private modalService: NgbModal,
    private resolve: ConsultaResolverService,
    private route: ActivatedRoute,
    private router: Router,
    public breakpointObserver: BreakpointObserver
  ) {
  }

  ngOnInit(): void {
    this.breakpointObserver
    .observe(['(max-width: 1126px)'])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.showContainer = true;
      } else {
        this.showContainer = false;
      }
    });
    this.arquivos.sort((a: { dataCriacao: any; }, b: { dataCriacao: any; }) =>
    (b.dataCriacao).localeCompare(a.dataCriacao)
  );

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.infoSolicitation?.currentValue !== undefined) {
      switch (changes.infoSolicitation.currentValue.tipoEscritura) {
        case 'ESCRITURA_COMPRA_VENDA':
          this.translations = ESCRITURA_COMPRA_VENDA
          break
        case 'ESCRITURA_DOACAO':
          this.translations = ESCRITURA_DOACAO
      }
    }
  }

  getAssunto(tipo: string) {
    switch (tipo) {
      case 'MINUTA_PREVIA':
        return 'Minuta prévia';
        break;
      case 'ORCAMENTO':
        return 'Orçamento';
        break;
      default:
        return 'Outros';
        break;
    }
  }

  getStatus(tipo: any) {
    if(tipo == null){
      return 'Enviado'
    }else if(tipo == true){
      return 'Aprovado'
    }else{
      return 'Reprovado'
    }
  }



  download(doc: any) {
    doc.documentos.forEach((element: any) => {
        window.open(element, '_blank');
    });
  }

  docValidation(doc: any, aprovation:any){
    const modalRef = this.modalService.open(ConfirmValidacaoComponent, { });
    modalRef.componentInstance.doc = doc;
    modalRef.componentInstance.aprovation = aprovation;
    modalRef.componentInstance.infoSolicitation = this.infoSolicitation;
    modalRef.result.then(res => {
        this.resolve.resolve(this.route.snapshot, this.router.routerState.snapshot)?.subscribe(res => {
          this.refresh.emit(res)
        })
    })
}

}
