import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/services/api.service';
import { ConsultaResolverService } from '../../../consulta-resolver.service';

@Component({
  selector: 'app-confirm-validacao',
  templateUrl: './confirm-validacao.component.html',
  styleUrls: ['./confirm-validacao.component.scss']
})
export class ConfirmValidacaoComponent implements OnInit {
  public motivo: string = ''

  @Input() doc: any
  @Input() infoSolicitation: any
  @Input() aprovation: any
  @Output() refresh = new EventEmitter()

  constructor(
    public activeModal: NgbActiveModal,
    private api: ApiService,

  ) { }

  ngOnInit(): void {

  }

  submit(status: any){
    if(status == true){
      const json = {
        codigoInterno: this.infoSolicitation.codigoInterno,
        codigo: this.doc.codigo,
        valido: this.aprovation,
      }
        this.api.postApi(`/solicitacao/aprovar-documento`, json).subscribe(res => {
          this.activeModal.close()
        })
    }else{
      const json = {
        codigoInterno: this.infoSolicitation.codigoInterno,
        codigo: this.doc.codigo,
        valido: this.aprovation,
        obs: this.motivo
      }
      this.api.postApi(`/solicitacao/aprovar-documento`, json).subscribe(res => {
        this.activeModal.close()
      })
    }
  }
}
