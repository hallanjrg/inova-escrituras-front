import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-solicitor-data',
  templateUrl: './solicitor-data.component.html',
  styleUrls: ['./solicitor-data.component.scss']
})
export class SolicitorDataComponent implements OnInit {
  public showContainer: any;
  @Input() solicitor: any
  @Input() infoSolicitation: any

  constructor(public breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(max-width: 950px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainer = true;
        } else {
          this.showContainer = false;
        }
      });
  }

}
