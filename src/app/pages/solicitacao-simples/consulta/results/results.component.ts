import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  @Input() infoSolicitation: any
  @Input() bens: any
  @Input() compradores: any
  @Input() vendedores: any
  @Input() solicitor: any
  @Input() concerneds: any
  @Input() pendencias: any
  @Input() chat: any
  @Input() doadores: any
  @Input() beneficiarios: any
  @Input() arquivos: any

  @Output() refresh = new EventEmitter();

  public collapsed: boolean = true;

  constructor() {
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.chat.currentValue && changes.chat?.currentValue.length > 0) {
      this.collapsed = false
    } else {
      this.collapsed = true
    }
  }

  updateData(e: any) {
    this.refresh.emit(e)
  }

}
