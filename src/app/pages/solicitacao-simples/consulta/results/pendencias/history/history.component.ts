import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  @Input() history: any

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    let messageBody = document.querySelector('#modal-body-id')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
      }, 1);
    }
  }

  baixarArquivo(link:any){
    window.open(link)
  }


}
