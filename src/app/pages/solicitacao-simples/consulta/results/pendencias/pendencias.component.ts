import { Component, Input, OnInit, Output, EventEmitter, TRANSLATIONS, SimpleChanges, DebugElement } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router, RouterState, RouterStateSnapshot } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NovaPendenciaComponent } from '../../../components/nova-pendencia/nova-pendencia.component';
import { ConsultaResolverService } from '../../consulta-resolver.service';
import { HistoryComponent } from './history/history.component';
import { ESCRITURA_COMPRA_VENDA, ESCRITURA_DOACAO } from '../../../../../models/translation'
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-pendencias',
  templateUrl: './pendencias.component.html',
  styleUrls: ['./pendencias.component.scss']
})
export class PendenciasComponent implements OnInit {
  public showContainer: any;
  @Input() pendencias: any
  @Input() infoSolicitation: any
  @Output() refresh = new EventEmitter()

  public translations: any = ESCRITURA_COMPRA_VENDA

  constructor(private modalService: NgbModal,
    private resolve: ConsultaResolverService,
    private route: ActivatedRoute,
    private router: Router,
    public breakpointObserver: BreakpointObserver) {
  }

  ngOnInit(): void {
    this.breakpointObserver
    .observe(['(max-width: 950px)'])
    .subscribe((state: BreakpointState) => {
      if (state.matches) {
        this.showContainer = true;
      } else {
        this.showContainer = false;
      }
    });
    this.pendencias.sort((a: { dataPendencia: any; }, b: { dataPendencia: any; }) =>
      (b.dataPendencia).localeCompare(a.dataPendencia)
    );

  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.infoSolicitation?.currentValue !== undefined) {
      switch (changes.infoSolicitation.currentValue.tipoEscritura) {
        case 'ESCRITURA_COMPRA_VENDA':
          this.translations = ESCRITURA_COMPRA_VENDA
          break
        case 'ESCRITURA_DOACAO':
          this.translations = ESCRITURA_DOACAO
      }
    }
  }

  /* getResolvido(pendencia: any){

    let resolvido: any = []
  pendencia.forEach((element: any) => {
    if (element.resolvido != true) {
      resolvido.push(element);
      return resolvido
    }
  });

  } */

  readAsNumber(r: string) {
    return Number(r)
  }

  resolverPendencia(pendencia: any) {
    const modalRef = this.modalService.open(NovaPendenciaComponent, { modalDialogClass: 'w-100 p-4' });
    modalRef.componentInstance.pendencia = pendencia;
    modalRef.result.then(res => {
      if (res) {
        this.resolve.resolve(this.route.snapshot, this.router.routerState.snapshot)?.subscribe(res => {
          this.refresh.emit(res)
        })
      }
    })
  }

  openHistory(index: number) {
    const modalRef = this.modalService.open(HistoryComponent, {

    });
    modalRef.componentInstance.history = this.pendencias[index].mensagens;
  }

  findIfThereIsPendingByType(number: number) {
    let typesToShow: any = []
    if (this.pendencias) {
      this.pendencias.forEach((element: any) => {
        if (element.tipoArquivo.charAt(2) === String(number)) {
          typesToShow.push(element)
        }
      });
      return typesToShow.length
    }
  }

  enablePendingItemsNotification(number: number) {
    let typesToShow: any = []
    let pendenciasNoType = []
    if (number === 0) {
      if (this.pendencias) {
        this.pendencias.forEach((element: any) => {
          if (element.tipoArquivo.charAt(2) === String(number) && element.resolvido === false) {
            pendenciasNoType.push(element)
          }
        });
        if (pendenciasNoType.length > 0) {
          return true
        }
      }
    } else {
      if (this.pendencias) {
        this.pendencias.forEach((element: any) => {
          if (element.tipoArquivo.charAt(2) === String(number) && element.resolvido === false) {
            typesToShow.push(element)
          }
        });
        if (typesToShow.length > 0) {
          return typesToShow.length
        }
      }
    }
    return false

  }

}
