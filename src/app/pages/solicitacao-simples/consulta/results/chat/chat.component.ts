import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  public files: Array<any> = []
  public filesShow: Array<any> = [];
  public form: FormGroup
  public message: string = ''
  @Input() chat: any
  @Input() infoSolicitation: any
  @Input() collapsed: any

  constructor(private api: ApiService,
    private fb: FormBuilder,
  ) {
    this.form = this.fb.group({
      codigoSolicitacao: ['', Validators.required],
      mensagem: [''],
      arquivo: ['']
    })
  }

  ngOnInit(): void {
    this.loadMessage()
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.collapsed?.previousValue !== undefined) {
      if (this.collapsed === false) {
        let messageBody = document.querySelector('#collapseChatTeste')
        if (messageBody) {
          setTimeout(() => {
            messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
          }, 1);
        }
      }
    }
  }

  sendMessage() {
    if(this.form.value.arquivo == ""){
      let mensagem = [];
      this.form.get('arquivo')?.setValue(mensagem)
    }
    this.api.postApi(`/chat/enviar`, this.form.value).subscribe(res => {
      this.loadMessage()
    })
  }

  download(link: string) {
    window.open(link, '_blank')
  }


  loadMessage() {
    this.form = this.fb.group({
      codigoSolicitacao: [this.infoSolicitation.codigoInterno, Validators.required],
      mensagem: [''],
      arquivo: ['']
    })
    this.removeFile()
    this.api.getApi(`/chat/listar?codigoSolicitacao=${this.infoSolicitation.codigoInterno}`).subscribe(res => {
      this.chat = res
      this.scrollBottom()
    })
  }

  scrollBottom(){
    let messageBody = document.querySelector('#collapseChatUp')
    if (messageBody) {
      setTimeout(() => {
        messageBody!.scrollTop = messageBody!.scrollHeight - messageBody!.clientHeight
      }, 1);
    }
  }




  handleSelect(event: any) {
    let array = Array.from(event.target.files)
    array.forEach(element => {
      this.upload(element)
    })
    event.target.value = ''
  }

  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.api.post('/bucket/upload', formData).subscribe((res: any) => {
      this.files.push(res.codigoArquivo)
      this.form.get('arquivo')?.setValue(this.files)
      this.filesShow.push({ nome: fileList.name })
    }, err => {
    })
  }

  removeSelectedIndex(index){
    this.files.splice(index,1)
    this.filesShow.splice(index,1)
  }

  removeFile() {
    this.files = []
    this.filesShow = []
    this.form.get('arquivo')?.setValue('')
  }

}
