import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ApiService } from 'src/app/services/api.service';
import { first } from 'rxjs/operators';
import { ConsultaResolverService } from './consulta-resolver.service';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.scss']
})
export class ConsultaComponent implements OnInit {

  public form: FormGroup
  public data: any = null;
  public error = {
    email: {
      message: ''
    }
  }

  get notFound() {
    return this.resolver.err
  }

  constructor(
    private route: Router,
    private router: ActivatedRoute,
    private resolver: ConsultaResolverService,
    private fb: FormBuilder) {
    const { codigo, email } = router.snapshot.queryParams
    this.form = this.fb.group({
      number: codigo,
      email: email
    })
    router.data.subscribe(({ data }) => {
      this.data = data
    });
  }

  ngOnInit(): void {
  }

  updateData(e: any) {
    this.data = e

  }

  search() {
    if (this.form.invalid) {
      alert('Verifique os campos inválidos')
    } else this.route.navigate([], {
      queryParams: {
        codigo: this.form.value.number,
        email: this.form.value.email,
        codigoCartorio: this.router.snapshot.queryParams['codigoCartorio']
      }
    })
  }

  validacaoEmail(field: string) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.form.controls.email.setErrors(null)
      } else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.form.controls.email.setErrors({ 'invalid': true })
      }
    }
  }



}
