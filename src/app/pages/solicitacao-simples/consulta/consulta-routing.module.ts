import { resolve } from '@angular/compiler-cli/src/ngtsc/file_system';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultaResolverService } from './consulta-resolver.service';
import { ConsultaComponent } from './consulta.component';

const routes: Routes = [
  {
    path: ':id',
    component: ConsultaComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      data: ConsultaResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [ConsultaResolverService]
})
export class ConsultaRoutingModule { }
