import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Funcionality } from 'src/app/models/config';
import { SolicitacaoSimples } from 'src/app/models/solicitacaoSimples';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import validator from 'validar-telefone';

interface Types {
  chave: string;
  descricao: string;
  descricaoGrupoServico: string;
  grupoServico: string;
}
@Component({
  selector: 'app-solicitacao-simples',
  templateUrl: './solicitacao-simples.component.html',
  styleUrls: ['./solicitacao-simples.component.scss']
})
export class SolicitacaoSimplesComponent implements OnInit {
  public cartorio: any = []
  public tipoEscritura: any;
  public grupoServico: any;
  public files: Array<any> = [];
  public urlLogo: any;
  public responsible: any;
  public menu: any;
  public config: any;
  public codigoEquipe: any;
  public optionResponsible: boolean = false;
  public filesShow: Array<any> = [];
  public codigoResponsavel: any;
  public form: FormGroup
  public codigoCartorio = ''
  public types: Array<Types> = []
  public error = {
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }
  public disabledButton: boolean = true;
  public optInOption: boolean = false;
  public optIn: any;
  public politicaUso = false;
  public politicaPrivacidade = false;
  public dominio: any;
  public toasts:any;

  constructor(private fb: FormBuilder,
    private toastService: ToastService,
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private loadingService: LoadingService) {
    this.menu = this.route.snapshot.queryParams['m'];
    this.codigoCartorio = this.route.snapshot.params['id'];
    this.tipoEscritura = this.route.snapshot.queryParams['tipoSolicitacao'];
    this.grupoServico = this.route.snapshot.queryParams['grupoServico'];
    this.codigoResponsavel = this.route.snapshot.queryParams['codigoResponsavel'];
    this.codigoEquipe = this.route.snapshot.queryParams['codigoEquipe'];
    this.form = this.fb.group({
      ...SolicitacaoSimples.new(this.tipoEscritura ? this.tipoEscritura : null)
    })
  }


  ngOnInit(): void {
    this.functionalityFilter();
    this.getCartorio();
    this.getOptIn();
    this.getResponsible();
  }

  functionalityFilter() {
    if (this.tipoEscritura) {
      this.api.getApi(`/funcionalidades/listar`).subscribe((res: any) => {
        this.config = res;
        this.types = this.config.filter((funcionality: Funcionality) => funcionality.chave === this.tipoEscritura);
        this.form.get('tipoEscritura')?.disable();
        this.changetipoEscritura();
      })
    } else if (this.grupoServico) {
      this.api.getApi(`/cartorio/obter-configuracoes?codigoCartorio=${this.codigoCartorio}`).subscribe((res: any) => {
        this.config = res;
        this.types = this.config.funcionalidades.filter((funcionality: Funcionality) => funcionality.grupoServico === this.grupoServico)
        this.form.get('tipoEscritura')?.setValue(this.types[0].chave)
        this.changetipoEscritura()
      })
    } else if (this.codigoEquipe) {
      this.api.getApi(`/equipe-cartorio/listar`).subscribe((res: any) => {
        this.config = res.find((p: any) => p.id === this.codigoEquipe)
        this.types = this.config.funcionalidades
        this.changetipoEscritura()
      })
    } else {
      this.api.getApi(`/cartorio/obter-configuracoes?codigoCartorio=${this.codigoCartorio}`).subscribe((res: any) => {
        this.types = res.funcionalidades
        this.changetipoEscritura()
      })
    }
  }

  getCartorio() {
    this.api.getApi(`/cartorio/get`).subscribe((res: any) => {
      this.urlLogo = res.urlLogo;
      this.dominio = res.dominio
    })
  }

  getOptIn() {
    this.api.getApi(`/cartorio/listar-opt-in?codigoCartorio=${this.codigoCartorio}`).subscribe((res: any) => {
      this.optIn = res
    })
  }

  getResponsible() {
    this.api.getApi(`/usuario/listar-recebedosite`).subscribe((res: any) => {
      this.responsible = res
    })
  }

  optionsResponsible(condition: boolean) {
    if (condition != null) {
      this.optionResponsible = condition
    }
  }

  phoneNumberIsReal(data: string) {
    if (data && data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        this.form.controls.telefoneSolicitante.setErrors({ 'invalid': true })
        this.form.controls.telefoneSolicitante.markAsTouched()
      } else {
        this.error.phone.message = ''
      }
    }
  }

  changetipoEscritura() {
    if (this.optIn) {
      this.optInOption = this.optIn.tipoServico.find(p => p == this.form.value.tipoEscritura) ? true : false
    }
    if (this.form.value.tipoEscritura && this.form.controls.tipoEscritura.value.includes('CERTIDAO_REGISTRO') as any) {
      let link = `https://escrituras.${this.dominio}/registro-imoveis/${this.codigoCartorio}`;
      window.location.href = link;
    } else if (this.form.value.tipoEscritura && this.form.controls.tipoEscritura.value.includes('CERTIDAO') as any) {
      let link = `https://solicitacoes.inovacartorios.com.br/${this.form.controls.tipoEscritura.value.replace("CERTIDAO_", "").toLowerCase() == 'escritura' ? 'escrituras' : this.form.controls.tipoEscritura.value.replace("CERTIDAO_", "").toLowerCase()}?codigoCartorio=${this.codigoCartorio}`;
      window.location.href = link;
    }
    let nomeSolicitante = document.getElementById('nomeSolicitante');
    let telefoneSolicitante = document.getElementById('telefoneSolicitante');
    let emailSolicitante = document.getElementById('emailSolicitante');
    this.form.controls.emailSolicitante.markAsUntouched()
    this.form.controls.nomeSolicitante.markAsUntouched()
    this.form.controls.telefoneSolicitante.markAsUntouched()
    if (this.form.controls.tipoEscritura.value == "FALE_CONOSCO_RECLAMACAO" || this.form.controls.tipoEscritura.value == "FALE_CONOSCO_DENUNCIA" || this.form.controls.tipoEscritura.value == "FALE_CONOSCO_OUVIDORIA") {
      emailSolicitante.removeAttribute("required");
      this.form.controls.emailSolicitante.setErrors(null)
      nomeSolicitante.removeAttribute("required");
      this.form.controls.nomeSolicitante.setErrors(null)
      telefoneSolicitante.removeAttribute("required");
      this.form.controls.telefoneSolicitante.setErrors(null)
    } else {
      emailSolicitante.setAttribute("required", "true");
      this.form.controls.emailSolicitante.setErrors({ 'required': true })
      nomeSolicitante.setAttribute("required", "true");
      this.form.controls.nomeSolicitante.setErrors({ 'required': true })
      telefoneSolicitante.setAttribute("required", "true");
      this.form.controls.telefoneSolicitante.setErrors({ 'required': true })
    }
  }

  validacaoEmail(field: string) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        this.form.controls.emailSolicitante.setErrors(null)
      } else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        this.form.controls.emailSolicitante.setErrors({ 'invalid': true })
      }
    }
  }

  validate() {
    let errors = []
    let b = this.form.controls
    if (b) {
      for (let name in b) {
        if (b[name].invalid) {
          b[name].markAsTouched()
          errors.push(name)
        }
      }
    }
  }

  submit() {
    this.toastService.removeAll();
    if (this.form.invalid) {
      this.validate();
      if (this.optIn && this.optInOption) {
        if (!this.politicaUso && !this.politicaPrivacidade) {
          this.toasts = [this.toastService.show('Os campos de termos são obrigatórios', { classname: 'toast-warning' , delay: 5000}), this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning' , delay: 5000})]
        } else if (!this.politicaUso) {
          this.toasts = [this.toastService.show('O campo de termo de pólitica de uso é obrigatório', { classname: 'toast-warning', delay: 5000}), this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning', delay: 5000 })]
        } else if (!this.politicaPrivacidade) {
          this.toasts = [this.toastService.show('O campo de termo de pólitica de privacidade é obrigatório', { classname: 'toast-warning', delay: 5000 }), this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning', delay: 5000 })]
        } else {
          this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning', delay: 5000 })
        }
      } else {
        this.toasts = this.toastService.show('Os campos marcados em vermelho são obrigatórios.', { classname: 'toast-warning', delay: 5000 })
      }
    } else {
      if (this.optIn && this.optInOption) {
        if (this.politicaUso && this.politicaPrivacidade) {
          this.confirm();
        } else {
          !this.politicaUso && !this.politicaPrivacidade ? this.toasts = this.toastService.show('Os campos de termos são obrigatórios', { classname: 'toast-warning', delay: 5000 }) : !this.politicaUso ? this.toasts = this.toastService.show('O campo de termo de pólitica de uso é obrigatório', { classname: 'toast-warning' , delay: 5000}) : this.toasts = this.toastService.show('O campo de termo de pólitica de privacidade é obrigatório', { classname: 'toast-warning' , delay: 5000})
        }
      }
    }
  }

  confirm() {
    if (this.form.get('tipoEscritura').value == 'REGISTRO_TITULO_IMOVEIS') {
      this.form.get('codigoStatus').setValue(28)
    }
    if (this.codigoResponsavel) {
      this.form.get('codigoResponsavel').setValue(this.codigoResponsavel)
    }
    this.form.enable()
    this.api.postApi(`/solicitacao/save`, this.form.value).subscribe((res: any) => {
      this.disabledButton = false
      this.router.navigate([`solicitacao-simples/finish/${this.codigoCartorio}`],
        {
          queryParams: {
            codigoCartorio: this.codigoCartorio,
            codigoSimples: res.codigo,
            tipoSolicitacao: this.form.value.tipoEscritura
          }
        }
      )
    })
  }

  handleSelect(event: any) {
    this.toastService.removeAll();
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element)
      } else {
        this.toasts = this.toastService.show('O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido', { classname: 'toast-warning' , delay: 5000})
      }
    })
    event.target.value = ''
  }


  upload(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.api.post('/bucket/upload', formData).subscribe((res: any) => {
      this.loadingService.isActive = false;
      this.files.push(res.codigoArquivo);
      this.filesShow.push({ nome: fileList.name })
      this.form.get('arquivo')?.setValue(this.files);
    }, err => {
    })
  }

  deleteFiles(index: any) {
    this.files.splice(index, 1)
    this.filesShow.splice(index, 1)

  }
}
