import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistroImoveisComponent } from './registro-imoveis.component';
import { RouterModule } from '@angular/router';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';



@NgModule({
  declarations: [RegistroImoveisComponent],
  imports: [
    CommonModule,
    NgbNavModule,
    ReactiveFormsModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    RouterModule.forChild([
      { path: 'success', loadChildren: () => import('./success/success.module').then(m => m.SuccessModule) },
      { path: ':id', component: RegistroImoveisComponent },
    ])
  ]
})
export class RegistroImoveisModule { }
