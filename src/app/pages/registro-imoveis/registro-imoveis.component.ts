import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import { CertidaoRegistroImoveis } from 'src/app/models/certidao_registro_imoveis';
import { TRADUCAO_CERTIDAO } from 'src/app/models/traducao';
import { ViaCep } from 'src/app/models/viacep';
import { ApiService } from 'src/app/services/api.service';
import { LocationService } from 'src/app/services/location.service';
import { LoginService } from 'src/app/services/login.service';
import { ToastService } from 'src/app/services/toast/toast-service';
import validator from 'validar-telefone';

@Component({
  selector: 'app-registro-imoveis',
  templateUrl: './registro-imoveis.component.html',
  styleUrls: ['./registro-imoveis.component.scss']
})
export class RegistroImoveisComponent implements OnInit {

  certidaoVazia = false;
  descricaoVazia = false;
  traducaoCertidao = TRADUCAO_CERTIDAO;
  tipoCertidao: string = '';
  descricao: string = '';
  formaDeEntrega: number = 1;
  enderecoDeEntrega: number = 1;
  codigoCartorio: string = '';
  correioDeDelivery: number = 1;
  area: number = 1;
  frete: number = 0;
  valorTotal: number = 0;
  tiposCertidao = [];
  valoresCertidao;
  incluirMaisCertidoes: boolean = false;
  active = 1;
  toast;
  arquivos: Array<string> = [];
  public optInOption: boolean = false;
  public optIn: any;
  public politicaUso: boolean = false;
  public politicaPrivacidade: boolean = false;

  dados: FormGroup = new FormGroup({
    canal: new FormControl('2'),
    tipoEscritura: new FormControl('CERTIDAO_REGISTRO_IMOVEIS'),
    ignorarValidacao: new FormControl(true),
    detalhes: new FormControl('', Validators.required),
    nomeSolicitante: new FormControl('', Validators.required),
    emailSolicitante: new FormControl('', Validators.required),
    codigoStatus: new FormControl(1, Validators.required),
    codigoEquipe: new FormControl('', Validators.required),
    codigoResponsavel: new FormControl('', Validators.required),
    telefoneSolicitante: new FormControl('', Validators.required),
    cpfCnpj: new FormControl('', Validators.required),
    mensagem: new FormControl('', Validators.required),
    retirada: new FormControl(1, Validators.required),
    tipoEntrega: new FormControl(1, Validators.required),
    cep: new FormControl('', Validators.required),
    logradouro: new FormControl('', Validators.required),
    numero: new FormControl('', Validators.required),
    complemento: new FormControl(''),
    cidade: new FormControl('', Validators.required),
    estado: new FormControl('', Validators.required),
    enderecoEntrega: new FormControl(1),
    cepEntrega: new FormControl('', Validators.required),
    logradouroEntrega: new FormControl('', Validators.required),
    numeroEntrega: new FormControl('', Validators.required),
    complementoEntrega: new FormControl(''),
    cidadeEntrega: new FormControl('', Validators.required),
    estadoEntrega: new FormControl('', Validators.required),
    areaEntrega: new FormControl(1, Validators.required),
    certidoes: new FormArray([]),
    frete: new FormControl(0, Validators.required),
    valorSolicitacao: new FormControl(0, Validators.required)
  });

  get certidoes() {
    return this.dados.get('certidoes') as FormArray
  }

  get estados() {
    return this.location.estados
  }

  get municipios() {
    return this.location.municipios
  }

  constructor(private router: Router,
    private toastService: ToastService,
    private api: ApiService,
    private location: LocationService,
    private route: ActivatedRoute,
    private login: LoginService) {
    this.codigoCartorio = this.route.snapshot.params.id
    }

  ngOnInit(): void {
    this.location.getEstados();
    this.listarTipoCertidao()
    this.login.getCartorio();
    this.getOptionValue();
  }

  async getOptionValue() {
    this.optIn = await this.api.getApi(`/cartorio/listar-opt-in?codigoCartorio=${this.codigoCartorio}`).toPromise();
    if (this.optIn) {
      this.optInOption = this.optIn.tipoServico.find(p => p == this.dados.get('tipoEscritura').value) ? true : false
    }
  }

  async listarTipoCertidao() {
    const tipos: any = await this.api.getApi('/solicitacao/listar-tipo-certidao').toPromise();
    const valores: any = await this.api.getApi(`/cartorio/obter-configuracoes?codigoCartorio=${this.codigoCartorio}`).toPromise();
    this.tiposCertidao = tipos;
    let obj = {};
    valores.valoresSolicitacoes.find(p => p.tipoSolicitacao == 'CERTIDAO_REGISTRO_IMOVEIS').tipoValor.map(valor => { obj[valor.chave] = valor })
    this.valoresCertidao = obj;
  }

  adicionarCertidao() {
    const certidao = new FormGroup({
      descricao: new FormControl(this.descricao),
      mensagem: new FormControl(''),
      tipoCertidao: new FormControl(this.tipoCertidao),
      tipoEntrega: new FormControl(''),
      arquivo: new FormArray([])
    })
    const arquivos = certidao.get('arquivo') as FormArray;
    this.arquivos.map(arquivo => {
      arquivos.push(new FormControl(arquivo))
    });
    this.certidoes.push(certidao);
  }

  excluirCertidao(index: number) {
    this.certidoes.removeAt(index)
  }

  areaEntrega(value) {
    this.area = value;
    this.dados.get('areaEntrega').setValue(value)
  }

  formaEntrega(value) {
    this.formaDeEntrega = value;
    this.dados.get('retirada').setValue(value)
    if(value == 1){
      this.frete = 0;
      this.dados.get('frete').setValue(0);
    }
  }

  translateFreight(){
    if(this.correioDeDelivery == 2){
     return this.area == 1 ? 'Delivery - Área urbana' : 'Delivery - Área rural'
    }else{
      return 'Frete'
    }
}

  enderecoEntrega(value) {
    this.enderecoDeEntrega = value;
    if (this.enderecoDeEntrega == 1) {
      this.replicarEndereco()
    } else {
      this.dados.get('cepEntrega').setValue('');
      this.dados.get('logradouroEntrega').setValue('');
      this.dados.get('numeroEntrega').setValue('');
      this.dados.get('complementoEntrega').setValue('');
      this.dados.get('cidadeEntrega').setValue('');
      this.dados.get('estadoEntrega').setValue('');
    }
  }

  replicarEndereco() {
    this.dados.get('cepEntrega').setValue(this.dados.get('cep').value);
    this.dados.get('logradouroEntrega').setValue(this.dados.get('logradouro').value);
    this.dados.get('numeroEntrega').setValue(this.dados.get('numero').value);
    this.dados.get('complementoEntrega').setValue(this.dados.get('complemento').value);
    this.dados.get('cidadeEntrega').setValue(this.dados.get('cidade').value);
    this.dados.get('estadoEntrega').setValue(this.dados.get('estado').value);
  }

  carregarEndereco(tipo?: number) {
    if (tipo == 1) {
      if (this.dados.get('cep').value.length == 8) {
        this.api.getCep(this.dados.get('cep').value).subscribe((endereco: ViaCep) => {
          this.dados.get('logradouro').setValue(endereco.logradouro);
          this.dados.get('cidade').setValue(endereco.localidade);
          this.dados.get('estado').setValue(endereco.uf);
          this.carregarCidades(1)
        })
      }
    } else if (tipo == 2) {
      if (this.dados.get('cepEntrega').value.length == 8) {
        this.api.getCep(this.dados.get('cepEntrega').value).subscribe((endereco: ViaCep) => {
          this.dados.get('logradouroEntrega').setValue(endereco.logradouro);
          this.dados.get('cidadeEntrega').setValue(endereco.localidade);
          this.dados.get('estadoEntrega').setValue(endereco.uf);
          this.carregarCidades(2)
        })
      }
    }
  }

  carregarCidades(tipo?: number) {
    if (tipo == 1) {
      this.location.getMunicipiosPorEstado(this.dados.get('estado').value)
    } else if (tipo == 2) {
      this.location.getMunicipiosPorEstado(this.dados.get('estadoEntrega').value)
    }
  }

  correioDelivery(value) {
    this.correioDeDelivery = value;
    this.dados.get('tipoEntrega').setValue(value)
  }

  success() {
    let i = window.location.pathname.lastIndexOf('/')
    let url = window.location.pathname.substr(i)
    const id = url.replace("/", "");
    this.dados.get('frete').setValue(this.frete);
    this.dados.get('valorSolicitacao').setValue(this.valorTotal);
    this.api.postApi('/solicitacao/save', this.dados.value).subscribe((retorno: CertidaoRegistroImoveis) => {
      this.dados.reset();
      this.router.navigate(['/registro-imoveis/success'], {
        queryParams: {
          codigo: retorno.codigo,
          id: id
        }
      })
    })
  }

  novaCertidao() {
    this.router.navigate(['/registro-imoveis'])
  }

  validarEmail(email: string) {
    let regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
    if (!regex.test(email)) {
      return false
    } else return true
  }

  validarCpfCnpj() {
    if (cpf.isValid(this.dados.get('cpfCnpj').value)) {
      return false
    } else if (cnpj.isValid(this.dados.get('cpfCnpj').value)) {
      return false
    } else return true
  }

  validarTelefone() {
    if (this.dados.get('telefoneSolicitante').value.length >= 10) {
      if (validator(this.dados.get('telefoneSolicitante').value)) {
        return false
      } else {
        return true
      }
    }
  }

  prosseguir() {
    this.toastService.remove(this.toast)
    switch (this.active) {
      case 1:
        if (this.tipoCertidao != '' && this.descricao != '') {
          if (this.incluirMaisCertidoes) {
            this.active = 5
            if (this.certidoes.length == 0 || this.incluirMaisCertidoes) {
              this.adicionarCertidao()
              this.incluirMaisCertidoes = false;
            }
          } else {
            this.active = 2
          }
        }
        if (this.tipoCertidao == '') {
          this.certidaoVazia = true;
        }
        if (this.descricao == '') {
          this.descricaoVazia = true;
        }
        break
      case 2:
        if (this.dados.get('nomeSolicitante').valid &&
          this.dados.get('emailSolicitante').valid &&
          this.dados.get('telefoneSolicitante').valid &&
          this.dados.get('cpfCnpj').valid &&
          this.dados.get('mensagem').valid) {
          if (this.validarEmail(this.dados.get('emailSolicitante').value) &&
            !this.validarTelefone() && !this.validarCpfCnpj()) {
            this.active = 3
          }

        } else {
          if (this.dados.get('nomeSolicitante').invalid) { this.dados.get('nomeSolicitante').markAsTouched() }
          if (this.dados.get('emailSolicitante').invalid) { this.dados.get('emailSolicitante').markAsTouched() }
          if (this.dados.get('telefoneSolicitante').invalid) { this.dados.get('telefoneSolicitante').markAsTouched() }
          if (this.dados.get('cpfCnpj').invalid) { this.dados.get('cpfCnpj').markAsTouched() }
          if (this.dados.get('mensagem').invalid) { this.dados.get('mensagem').markAsTouched() }
        }
        break
      case 3:
        if (this.dados.get('cep').valid &&
          this.dados.get('logradouro').valid &&
          this.dados.get('numero').valid &&
          this.dados.get('cidade').valid &&
          this.dados.get('estado').valid) {
          this.active = 4
          this.replicarEndereco()
        } else {
          if (this.dados.get('cep').invalid) { this.dados.get('cep').markAsTouched() }
          if (this.dados.get('logradouro').invalid) { this.dados.get('logradouro').markAsTouched() }
          if (this.dados.get('numero').invalid) { this.dados.get('numero').markAsTouched() }
          if (this.dados.get('cidade').invalid) { this.dados.get('cidade').markAsTouched() }
          if (this.dados.get('estado').invalid) { this.dados.get('estado').markAsTouched() }
        }
        break
      case 4:
        if (this.formaDeEntrega == 1) {
          this.active = 5
        }
        if (this.formaDeEntrega == 2) {
          if(this.correioDeDelivery !=2){
            this.calcularFrete();
          }
          if (this.enderecoDeEntrega == 2) {
            if (this.dados.get('cepEntrega').valid &&
              this.dados.get('logradouroEntrega').valid &&
              this.dados.get('numeroEntrega').valid &&
              this.dados.get('cidadeEntrega').valid &&
              this.dados.get('estadoEntrega').valid) {
              this.active = 5
            } else {
              if (this.dados.get('cepEntrega').invalid) { this.dados.get('cepEntrega').markAsTouched() }
              if (this.dados.get('logradouroEntrega').invalid) { this.dados.get('logradouroEntrega').markAsTouched() }
              if (this.dados.get('numeroEntrega').invalid) { this.dados.get('numeroEntrega').markAsTouched() }
              if (this.dados.get('cidadeEntrega').invalid) { this.dados.get('cidadeEntrega').markAsTouched() }
              if (this.dados.get('estadoEntrega').invalid) { this.dados.get('estadoEntrega').markAsTouched() }
              return
            }
          } if (this.correioDeDelivery == 1) {
            this.active = 5
          } if (this.correioDeDelivery == 2) {
            if (this.area) { this.valorDelivery() }
          }
        }
        if (this.certidoes.length == 0 || this.incluirMaisCertidoes) {
          this.adicionarCertidao()
          this.incluirMaisCertidoes = false;
        }
        break
      case 5:
          if (this.optIn && this.optInOption) {
            if (this.politicaUso && this.politicaPrivacidade) {
              this.success();
            } else {
              !this.politicaUso && !this.politicaPrivacidade ? this.toast = this.toastService.show('Os campos de termos são obrigatórios', { classname: 'toast-warning' }) : !this.politicaUso ? this.toast = this.toastService.show('O campo de termo de pólitica de uso é obrigatório', { classname: 'toast-warning' }) : this.toast = this.toastService.show('O campo de termo de pólitica de privacidade é obrigatório', { classname: 'toast-warning' })
            }
          }else{
            this.success();
          }
        break
    }
  }

  valorDelivery() {
    let valorFrete = 0;
    this.area == 1 ? valorFrete = this.valoresCertidao.VALOR_DELIVERY_URBANO.valor : valorFrete = this.valoresCertidao.VALOR_DELIVERY_RURAL.valor
    this.frete = valorFrete;
    this.active = 5
  }

  adicionarMais() {
    this.active = 1;
    this.incluirMaisCertidoes = true;
    this.descricao = '';
    this.tipoCertidao = '';
    this.arquivos = [];
  }

  async calcularFrete() {
    const freight: any = await this.location.getFreight(this.dados.get('cepEntrega')) as any;
    this.frete = freight.valor;
  }


  somarValoresCertidao() {
    let valor = 0;
    for (let certidao of this.certidoes.controls) {
      if (this.valoresCertidao[certidao.get('tipoCertidao').value]) {
        valor += this.valoresCertidao[certidao.get('tipoCertidao').value].valor
      } else valor += 0;
    };
    this.valorTotal = valor;
    return valor + this.frete;

  }

  selecionarArquivo(event: any) {
    let array = Array.from(event.target.files);
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.uploadArquivo(element);
      } else {
        this.toast = this.toastService.show(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`,
          { classname: 'text-center' });
      }
    })
    event.target.value = '';
  }

  uploadArquivo(fileList: any) {
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.api.postApiUpload('/bucket/upload', formData).subscribe((res: any) => {
      this.arquivos.push(res.codigoArquivo);


    }, err => {
      this.toast = this.toastService.show('Ocorreu um erro ao fazer o upload do arquivo. Tente novamente mais tarde.',
        { classname: 'text-center' })
    })
  }


}
