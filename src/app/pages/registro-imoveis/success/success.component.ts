import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  codigo;
  id;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.codigo = this.route.snapshot.queryParamMap.get('codigo');
    this.id = this.route.snapshot.queryParamMap.get('id');
  }

  voltarInicio() {
    this.router.navigate([`/registro-imoveis/${this.id}`])
  }


}
