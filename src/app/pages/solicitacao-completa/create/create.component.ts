import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Stepper from 'bs-stepper';
import { Asset, Concerned, Solicitation, Solicitor } from 'src/app/models/solicitacaoSimples';
import { Step } from 'src/app/models/stepHeader';
import { TYPOLOGY } from 'src/app/models/types';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  public solicitation: Solicitation = new Solicitation()
  public stepper: Stepper & { _currentIndex?: number } | any;
  public quantity: number = 0
  public types = TYPOLOGY

  public steps: Array<Step> = [
    {
      target: '#test-l-1',
      number: '1',
      title: 'Dados do ',
    },
    {
      target: '#test-l-2',
      number: '2',
      title: 'Dados do bem'
    },
    {
      target: '#test-l-3',
      number: '3',
      title: 'Dados do '
    },
    {
      target: '#test-l-4',
      number: '4',
      title: 'Dados do '
    },
    {
      target: '#test-l-5',
      number: '5',
      title: 'Resumo'
    }
  ]

  public qtdConcerned: Array<Concerned> = []
  public qtdAsset: Array<Asset> = []


  getCurrentStep() {
    if (this.stepper) {
      return this.stepper._currentIndex
    } else {
      return false
    }
  }

  constructor(route: ActivatedRoute) {
    this.solicitation.tipoEscritura = route.snapshot.queryParams['tipoEscritura']
  }

  ngOnInit(): void {
    this.loadStepper()
  }

  loadStepper() {
    setTimeout(() => {
      this.stepper = new Stepper(document.querySelector('#stepperPayroll') as Element, {
        linear: true,
        animation: true
      });
    }, 0);
  }

  updateForm(solicitor: Solicitor) {
    this.solicitation.solicitor = solicitor
  }

  next() {
    this.solicitation.envolvidos = Object.assign({}, this.qtdConcerned, this.qtdAsset)
    this.quantity = 0
    this.stepper.next()
  }

  previous() {
    this.solicitation.envolvidos = Object.assign({}, this.qtdConcerned, this.qtdAsset)

    this.stepper.previous()
  }


  defineQuantity(count: Number, type: string, concernedType: string) {
    if (type === 'concerned') {
      this.qtdConcerned = []
      for (let index = 0; index < count; index++) {
        this.qtdConcerned.push(Concerned.new(concernedType))
      }
    } else if (type === 'asset') {
      this.qtdAsset = []
      for (let index = 0; index < count; index++) {
        this.qtdAsset.push(new Asset())
      }
    }
  }


}
