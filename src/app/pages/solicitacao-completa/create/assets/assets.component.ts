import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Asset, Solicitation } from 'src/app/models/solicitacaoSimples';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.scss']
})
export class AssetsComponent implements OnInit {

  @Input() step: number = 1
  @Input() index: number = 0
  @Input() solicitation: Solicitation | undefined
  @Input() concernedType: any
  @Input('asset') asset: Asset = new Asset()
  @Output() update = new EventEmitter()
  @Output() goBack = new EventEmitter()
  @Output() goForward = new EventEmitter()


  public form: any
  public quantity: number = 0

  get errors() {
    return this.validation.error
  }

  constructor(
    public validation: ValidationService) {
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.concernedType = changes.concernedType?.currentValue
  }

  next() {
    this.goForward.emit()
/*     this.update.emit(this.form.value)
 */  }

  previous() {
    this.goBack.emit()
  }

}
