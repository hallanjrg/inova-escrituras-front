import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {

  @Input() step: number = 1
  @Output() update = new EventEmitter()
  @Output() goBack = new EventEmitter()
  @Output() goForward = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

  next() {
    this.goForward.emit()
/*     this.update.emit(this.form.value)
 */  }

  previous() {
    this.goBack.emit()
  }


}
