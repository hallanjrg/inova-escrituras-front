import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Solicitation } from 'src/app/models/solicitacaoSimples';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-solicitor-data',
  templateUrl: './solicitor-data.component.html',
  styleUrls: ['./solicitor-data.component.scss']
})
export class SolicitorDataComponent implements OnInit {

  public form: FormGroup
  @Input() step: number = 0
  @Output() update = new EventEmitter()
  @Output() goBack = new EventEmitter()
  @Output() goForward = new EventEmitter()

  get errors() {
    return this.validation.error
  }

  constructor(private fb: FormBuilder,
    public validation: ValidationService) {
    this.form = this.fb.group({
      ...Solicitation.new().solicitor
    })
  }

  ngOnInit(): void {
  }

  next() {
    this.goForward.emit()
    this.update.emit(this.form.value)
  }

  previous() {
    this.goBack.emit()
  }

}
