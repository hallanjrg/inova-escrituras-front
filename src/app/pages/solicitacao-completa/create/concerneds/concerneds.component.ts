import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ControlContainer, NgForm, NgModelGroup } from '@angular/forms';
import { Concerned, Solicitation } from 'src/app/models/solicitacaoSimples';
import { ValidationService } from 'src/app/services/validation.service';

@Component({
  selector: 'app-concerneds',
  templateUrl: './concerneds.component.html',
  styleUrls: ['./concerneds.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }]

})
export class ConcernedsComponent implements OnInit {

  @Input() step: number = 1
  @Input() index: number = 0
  @Input() solicitation: Solicitation | any
  @Input() concernedType: any
  @Input('concerned') concerned: Concerned = new Concerned()
  @Output() update = new EventEmitter()
  @Output() goBack = new EventEmitter()
  @Output() goForward = new EventEmitter()

  public form: any
  public quantity: number = 0

  get errors() {
    return this.validation.error
  }

  constructor(
    public validation: ValidationService) {
  }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges) {
    this.concernedType = changes.concernedType?.currentValue
  }

  next() {
    this.goForward.emit()
/*     this.update.emit(this.form.value)
 */  }

  previous() {
    this.goBack.emit()
  }


}
