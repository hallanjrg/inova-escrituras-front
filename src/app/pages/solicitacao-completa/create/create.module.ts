import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import { SolicitorDataComponent } from './solicitor-data/solicitor-data.component';
import { ConcernedsComponent } from './concerneds/concerneds.component';
import { AdditionalInfoComponent } from './additional-info/additional-info.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResumeComponent } from './resume/resume.component';
import { AssetsComponent } from './assets/assets.component';


@NgModule({
  declarations: [CreateComponent,
    SolicitorDataComponent,
    ConcernedsComponent,
    AdditionalInfoComponent,
    ResumeComponent,
    AssetsComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class CreateModule { }
