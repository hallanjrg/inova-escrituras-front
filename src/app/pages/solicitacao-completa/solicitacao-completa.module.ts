import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SolicitacaoCompletaRoutingModule } from './solicitacao-completa-routing.module';
import { SolicitacaoCompletaComponent } from './solicitacao-completa.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [SolicitacaoCompletaComponent],
  imports: [
    CommonModule,
    SolicitacaoCompletaRoutingModule,
    RouterModule
  ]
})
export class SolicitacaoCompletaModule { }
