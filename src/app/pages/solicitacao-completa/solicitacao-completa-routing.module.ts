import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SolicitacaoCompletaComponent } from './solicitacao-completa.component';

const routes: Routes = [
  {
    path: '', component: SolicitacaoCompletaComponent,
    children: [
      { path: 'create/:id', loadChildren: () => import('./create/create.module').then(m => m.CreateModule) }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SolicitacaoCompletaRoutingModule { }
