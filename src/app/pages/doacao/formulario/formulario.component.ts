import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { AssetData } from 'src/app/models/assetData';
import { BuyerData } from 'src/app/models/buyerData';
import { ClausulasEspeciais } from 'src/app/models/clausulas-especiais';
import { SellerData } from 'src/app/models/sellerData';
import { ApiService } from 'src/app/services/api.service';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastService } from 'src/app/services/toast.service';
import validator from "validar-telefone";
import { ConfirmDoacao } from './../../../models/dataToSendCompraVenda';

interface RetornoArquivo {
  codigoArquivo: string;
}

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
  public disabledButton: boolean = true;
  data: any
  durationInSeconds = 5;
  isLinear = false;
  public showContainer: any;
  public listFile: any = []
  public stepHasErrors = false
  public showClausulas: Array<ClausulasEspeciais> = []

  panelOpenState = false;

  confirm: ConfirmDoacao = {
    canal: '2',
    codigoCartorio: '',
    identificacao: '',
    nomeSolicitante: '',
    telefoneSolicitante: '',
    emailSolicitante: '',
    qtd: {
      qtdBens: '1',
      qtdDoadores: '1',
      qtdBeneficiario: '1',
    },
    bens: [],
    doadores: [],
    beneficiarios: [],
    questoesEscritura: [],
    clausulasEscritura: [],
    tipoEscritura: 'ESCRITURA_DOACAO'
  }
  qtdBens: Array<AssetData> = []
  qtdDoadores: Array<BuyerData> = []
  qtdBeneficiario: Array<SellerData> = []
  resp: any
  listAllCategories: Array<any> = []

  public error = {
    nome: {
      message: ''
    },
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }

  @ViewChild('id') id: any
  @ViewChild('form') form: any
  @ViewChild('form2') form2: any
  @ViewChild('form3') form3: any
  @ViewChild('form4') form4: any

  constructor(
    public infoService: InfoService,
    public loadingService: LoadingService,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private toast: ToastService,
    public breakpointObserver: BreakpointObserver) {
  }

  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(max-width: 950px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.showContainer = true;
        } else {
          this.showContainer = false;
        }
      });

    this.data = this.route.snapshot.queryParams
    if(this.data?.codigoSimples !=null){
      this.apiService.getApi(`/solicitacao/get?codigo=${this.data?.codigoSimples}`).subscribe((res: any) => {

        this.confirm.codigoCartorio = res.codigoCartorio
        this.confirm.identificacao = res.identificacao
        this.confirm.emailSolicitante = res.emailSolicitante
        this.confirm.nomeSolicitante = res.nomeSolicitante
        this.confirm.telefoneSolicitante = res.telefoneSolicitante
        this.confirm.msg = res.msg
        this.confirm.codigo = res.codigo
        this.confirm.codigoInterno = res.codigoInterno
        this.confirm.codigoStatus = res.codigoStatus
      })
    }


    let e1 = { event: '', value: 1 }
    let e2 = { event: '', value: 1 }
    let e3 = { event: '', value: 1 }
    this.confirm.codigoCartorio = this.route.snapshot.params.id
    this.defineQuantity(1, 1)
    this.defineQuantity(1, 2)
    this.defineQuantity(1, 3)
    this.setMaxInfo(e1, 1);
    this.setMaxInfo(e2, 2);
    this.setMaxInfo(e3, 3);
    this.setClausulasEspeciais();
  }

  translateBensOnResume(codigo: any, arr: any) {
    let ret = arr?.find((item: any) => item.codigo === codigo)
    if (ret) {
      return ret.nome
    }
  }

  phoneNumberIsReal(confirm: any, type?: number, index?: number) {
    if (confirm.length >= 10) {
      if (!validator(confirm)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        switch (type) {
          case 0:
            this.id.form.controls['confirm.telefoneSolicitante'].setErrors({ 'invalid': true })
            break
          case 1:
            this.form2.form.controls[`doador.telefone${index}`].setErrors({ 'invalid': true })
            break
          case 2:
            this.form3.form.controls[`vendedor.telefone${index}`].setErrors({ 'invalid': true })
            break
        }
      } else {
        this.error.phone.message = ''
        switch (type) {
          case 0:
            this.id.form.controls['confirm.telefoneSolicitante'].setErrors(null)
            break
          case 1:
            this.form2.form.controls[`doador.telefone${index}`].setErrors(null)
            break
          case 2:
            this.form3.form.controls[`vendedor.telefone${index}`].setErrors(null)
            break
        }
      }
    } else {
      this.error.phone.message = 'Este campo é obrigatório.'
    }
  }

  validacaoEmail(field: string, type?: number, index?: number) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        switch (type) {
          case 0:
            this.id.form.controls['confirm.emailSolicitante'].setErrors(null)
            break
          case 1:
            this.form2.form.controls[`doador.email${index}`].setErrors(null)
            break
          case 2:
            this.form3.form.controls[`vendedor.email${index}`].setErrors(null)
            break
        }
      }
      else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        switch (type) {
          case 0:
            this.id.form.controls['confirm.emailSolicitante'].setErrors({ 'invalid': true })
            break
          case 1:
            this.form2.form.controls[`doador.email${index}`].setErrors({ 'invalid': true })
            break
          case 2:
            this.form3.form.controls[`vendedor.email${index}`].setErrors({ 'invalid': true })
            break
        }
      }
    }
  }

  validateBuyerSeller(type: number, name: string, index: number) {
    let c
    type == 1 ? c = this.form2?.form?.controls : c = this.form3?.form?.controls
    if (!!c && (c[`${name}${index}`]?.touched && c[`${name}${index}`]?.invalid)) {
      return true
    } else {
      return false
    }
  }

  defineQuantity(count: Number, type: Number) {
    if (type == 1) {
      this.qtdBens = []
    }
    if (type == 2) {
      this.qtdDoadores = []
    }
    if (type == 3) {
      this.qtdBeneficiario = []
    }
    for (let index = 0; index < count; index++) {
      switch (type) {
        case 1:
          this.qtdBens.push(new AssetData())
          break;
        case 2:
          this.qtdDoadores.push(new BuyerData())
          break;
        case 3:
          this.qtdBeneficiario.push(new SellerData())
      }
    }

  }

  getQtd() {
    let arr = []
    for (let index = 0; index < 20; index++) {
      arr.push(index + 1)
    }
    return arr
  }

  setIdentificacao(type?: any) {
    switch (type) {
      case '1':
        if (this.confirm.identificacao === '1') {
          this.qtdDoadores[0].nome = this.confirm.nomeSolicitante
          this.qtdDoadores[0].telefone = this.confirm.telefoneSolicitante
          this.qtdDoadores[0].email = this.confirm.emailSolicitante
        }
        break;
      case '2':
        if (this.confirm.identificacao === '2') {
          this.qtdBeneficiario[0].nome = this.confirm.nomeSolicitante
          this.qtdBeneficiario[0].telefone = this.confirm.telefoneSolicitante
          this.qtdBeneficiario[0].email = this.confirm.emailSolicitante
        }
        break;
    }
  }

  setBem() {
    this.apiService.get('/dominio/tipo-bem').subscribe((res: any) => {
      this.qtdBens.forEach(element => {
        element.tipos = res
      });
    })
  }

  setCategoriaDocumento() {
    this.apiService.get('/dominio/categoria-documento').subscribe((res: any) => {
      this.listAllCategories = res
    })
  }


  updateValue(event: any) {
    if (event.type === 'QUESTIONARIO') {
      this.confirm.questoesEscritura = event.value
    } else if (event.type === 'CLAUSULAS_ESPECIAIS') {
      this.confirm.clausulasEscritura = event.value
    }
  }

  showClausulasOnResume(e: Array<ClausulasEspeciais>) {
    this.showClausulas = e
  }


  setClausulasEspeciais() {
    this.apiService.get('/dominio/clausulas-escritura').subscribe((res: any) => {
    })
  }

  setDocumentList(index: number, typeDocument: string, user: any) {
    let codigo: number = 0
    this.listAllCategories.forEach(element => {
      if (user == 1) {
        if (element.nome == typeDocument) {
          codigo = element.codigo
        }
      } else {

        if (element.nome == ('PESSOA-' + typeDocument)) {
          codigo = element.codigo
        }
      }
    });
    this.loadingService.isActive = true
    this.apiService.get(`/dominio/tipo-documento?codigoCategoria=${codigo}&codigoCartorio=${this.confirm.codigoCartorio}`).subscribe((res: any) => {
      switch (user) {
        case 1:
          this.qtdBens[index].items = res
          this.loadingService.isActive = false
          break;
        case 2:
          this.qtdDoadores[index].items = res
          if(this.confirm.codigoCartorio == "81147fbf-14a0-11eb-8252-0a96c6f995dd" && typeDocument == "FISICA"){
            this.qtdDoadores[index].items?.splice(4,1)
          }
          this.loadingService.isActive = false
          break;
        case 3:
          this.qtdBeneficiario[index].items = res
          if (codigo == 6 && user == 3) {
            this.apiService.get(`/dominio/tipo-documento?codigoCategoria=7`).subscribe((res: any) => {
              this.qtdBeneficiario[index].items?.push(...res)
              if(this.confirm.codigoCartorio == "81147fbf-14a0-11eb-8252-0a96c6f995dd" && typeDocument == "FISICA"){
                this.qtdBeneficiario[index].items?.splice(4,1)
              }
              this.loadingService.isActive = false
            }, err => this.loadingService.isActive = false)
          } else if (codigo == 8 && user == 3) {
            this.apiService.get(`/dominio/tipo-documento?codigoCategoria=9`).subscribe((res: any) => {
              this.qtdBeneficiario[index].items?.push(...res)
              this.loadingService.isActive = false
            }, err => this.loadingService.isActive = false)
          }
          break
      }
    }, err => this.loadingService.isActive = false)
  }

  goBack(stepper: MatStepper) {
    stepper.previous()
  }

  goForward(stepper: MatStepper) {
    switch (stepper.selectedIndex) {
      case 0:
        this.phoneNumberIsReal(this.id.form.controls['confirm.telefoneSolicitante'].value, 0)
        if (this.id.form.invalid) {

          for (let c in this.id.form.controls) {
            this.id.form.controls[c].markAsTouched()
            this.stepHasErrors = true
          }
          this.toast.throwSnack('Os campos em vermelho estão inválidos')
        } else {
          this.setIdentificacao(this.confirm.identificacao)
          stepper.next()
        }
        break;
      case 1:
        if (this.form.form.invalid) {
          for (let c in this.form.form.controls) {
            this.form.form.controls[c].markAsTouched()
            this.stepHasErrors = true
          }
          this.toast.throwSnack('Os campos em vermelho estão inválidos')
        } else {
          stepper.next()
          this.stepHasErrors = false
        }
        break;
      case 2:
        if (this.form2.form.invalid) {
          for (let c in this.form2.form.controls) {
            this.form2.form.controls[c].markAsTouched()
            this.stepHasErrors = true
          }
          this.toast.throwSnack('Os campos em vermelho estão inválidos')
        } else {
          stepper.next()
          this.stepHasErrors = false
        }
        break;
      case 3:
        if (this.form3.form.invalid) {
          for (let c in this.form3.form.controls) {
            this.form3.form.controls[c].markAsTouched()
            this.stepHasErrors = true
          }
          this.toast.throwSnack('Os campos em vermelho estão inválidos')
        } else {
          stepper.next()
          this.stepHasErrors = false
        }
        break;
      case 4:
        if (this.validateClausulasQuestoes()) {
          this.stepHasErrors = true
          this.toast.throwSnack('Todas as questões precisam ser respondidas')
        } else {
          stepper.next()
          this.stepHasErrors = false
        }

        break;
      case 5:
        this.submit()
        break;
    }
  }
  openSnackBar(message: string,) {
    this._snackBar.open(message, 'Ok', {
      duration: this.durationInSeconds * 1000,
    });
  }

  setMaxInfo(qtd: any, type: number) {
    this.stepHasErrors = false
    if (qtd > 20) {
      this.openSnackBar(`A quantidade de ${type} não pode ser maior do que 20.`)
    } else {
      this.defineQuantity(Number(qtd.value), type)
      this.setBem()
      this.setCategoriaDocumento()
      this.setIdentificacao(type)
    }
  }

  step1Invalid() {
    if (!this.confirm.emailSolicitante ||
      !this.confirm.nomeSolicitante ||
      !this.confirm.telefoneSolicitante) {
      return true
    } else {
      return false
    }
  }

  deleteUnexpectedAttributes() {
    let docBens: any = []
    let docDoadores: any = []
    let docBeneficiarios: any = []
    this.qtdBens.forEach(element => {
      this.listAllCategories.forEach(element2 => {
        if (element2.nome == element.tipoBem) {
          element.tipoBem = element2.codigo
        }
      })
      Object.defineProperty(element, 'tipoImovel', {
        enumerable: true,
        configurable: true,
        writable: true,
        value: element.tipoBem
      });
      delete element.tipoBem
      delete element.items
      delete element.tipos
      delete element.listaTipoBem
      element.documentos.forEach(element2 => {
        docBens.push(element2.id)
      });
      element.documentos = docBens
      docBens = []
    });
    this.qtdDoadores.forEach(element => {
      delete element.items
      element.documentos.forEach(element2 => {
        docDoadores.push(element2.id)
      });
      element.documentos = docDoadores
      docDoadores = []
    });
    this.qtdBeneficiario.forEach(element => {
      delete element.items
      element.documentos.forEach(element2 => {
        docBeneficiarios.push(element2.id)
      });
      element.documentos = docBeneficiarios
      docBeneficiarios = []
    });
    this.confirm.questoesEscritura.forEach(element => {
      delete element.descricao
    })
    this.confirm.identificacao = Number(this.confirm.identificacao)
  }

  validateClausulasQuestoes() {
    let errors = []
    this.confirm.questoesEscritura.forEach(element => {
      if (element.resposta === null) {
        errors.push(element)
      }
    });
    if (errors.length > 0) {
      return true
    } else return false
  }

  async submit() {
    await this.deleteUnexpectedAttributes()
    this.loadingService.isActive = true
    this.confirm.bens = this.qtdBens
    this.confirm.doadores = this.qtdDoadores
    this.confirm.beneficiarios = this.qtdBeneficiario
    const data = {
      ...this.confirm
    }
    this.apiService.putApi('/solicitacao/update', data).subscribe((res: any) => {
      this.disabledButton =false
      this.loadingService.isActive = false
       this.router.navigate([`ESCRITURA_DOACAO/finish/${this.confirm.codigoCartorio}`], {
        queryParams: {
          codigoSimples: res,
          email: this.confirm.emailSolicitante
        }
      })
    }, err => {
      this.loadingService.isActive = false
    })
  }

  handleSelect(event: any, number: number, type: string, index: number) {
    let array = Array.from(event.target.files)
    array.forEach((element: any) => {
      if (element.size < 10000000) {
        this.upload(element, number, index)
      } else {
        this.openSnackBar(`O arquivo ${element.name} não foi adicionado pois excede o tamanho permitido`)
      }
    })
    event.target.value = ''
  }

  upload(fileList: any, number: number, index: number) {
    this.loadingService.isActive = true
    let formData = new FormData
    formData.append('arquivo', fileList);
    this.apiService.post('/bucket/upload', formData).subscribe((res: any) => {
      this.loadingService.isActive = false
      if (number === 1) {
        this.qtdBens[index].documentos.push({ id: res.codigoArquivo, nome: fileList.name } as any)
      } else if (number === 2) {
        this.qtdDoadores[index].documentos.push({ id: res.codigoArquivo, nome: fileList.name } as any)
      } else if (number === 3) {
        this.qtdBeneficiario[index].documentos.push({ id: res.codigoArquivo, nome: fileList.name } as any)
      }
    }, err => {
      this.loadingService.isActive = false
    })
  }

  deleteFile(index: number, number: number, file: any) {
    if (number == 1) {
      this.qtdBens[index].documentos.forEach(element => {
        if (element.id == file.id) {
          const i = element.id.indexOf(file.id)
          this.qtdBens[index].documentos.splice(i, 1)
        }
      });
    } else if (number == 2) {
      this.qtdDoadores[index].documentos.forEach(element => {
        if (element.id == file.id) {
          const i = element.id.indexOf(file.id)
          this.qtdDoadores[index].documentos.splice(i, 1)
        }
      });
    } else if (number == 3) {
      this.qtdBeneficiario[index].documentos.forEach(element => {
        if (element.id == file.id) {
          const i = element.id.indexOf(file.id)
          this.qtdBeneficiario[index].documentos.splice(i, 1)
        }
      });
    }
  }



}
