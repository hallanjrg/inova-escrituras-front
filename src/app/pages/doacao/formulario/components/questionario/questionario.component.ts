import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Questionario } from 'src/app/models/questionario';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-questionario',
  templateUrl: './questionario.component.html',
  styleUrls: ['./questionario.component.scss']
})
export class QuestionarioComponent implements OnInit {

  public questoes: Array<Questionario> = [];

  @Output() complete = new EventEmitter();

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.setQuestionario();
  }

  setQuestionario() {
    this.apiService.get('/dominio/questoes-escritura').subscribe((res: any) => {
      res.forEach((element: Questionario) => {
        this.addItem(element)
      });
      this.complete.emit({value: this.questoes, type: 'QUESTIONARIO'})
    })
  }

  addItem(item: Questionario) {
    this.questoes.push(item);
  }

  checkQuestion(event: any, questao: Questionario) {
    let i = this.questoes.findIndex(i => i.codigo === questao.codigo)
    this.questoes[i].resposta = event.value
    this.complete.emit({value: this.questoes, type: 'QUESTIONARIO'})
  }

  updateValue() {
    this.complete.emit({value: this.questoes, type: 'QUESTIONARIO'})

  }
}
