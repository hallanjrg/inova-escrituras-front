import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ClausulasEspeciais } from 'src/app/models/clausulas-especiais';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-clausulas-especiais',
  templateUrl: './clausulas-especiais.component.html',
  styleUrls: ['./clausulas-especiais.component.scss']
})
export class ClausulasEspeciaisComponent implements OnInit {

  public clausulas: Array<ClausulasEspeciais> = []
  public clausulasToSend: Array<Number> = []
  public clausulasToShow: Array<ClausulasEspeciais> = []

  @Output() complete = new EventEmitter();
  @Output() showClausulaOnResume = new EventEmitter();

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.setClausulas()
  }

  setClausulas() {
    this.apiService.get('/dominio/clausulas-escritura').subscribe((res: any) => {
      res.forEach((element: ClausulasEspeciais) => {
        this.addItem(element)
      });
    })
  }

  addItem(item: ClausulasEspeciais) {
    this.clausulas.push(item);
  }

  checkClausula(event: any, clausula: ClausulasEspeciais) {
    let i = this.clausulasToSend.findIndex(i => i === clausula.codigo)
    if (event.checked) {
      this.clausulasToSend.push(clausula.codigo)
      this.clausulasToShow.push(clausula)
      this.complete.emit({ value: this.clausulasToSend, type: 'CLAUSULAS_ESPECIAIS' })
      this.showClausulaOnResume.emit(this.clausulasToShow)
    } else {
      this.clausulasToSend.splice(i, 1)
      this.clausulasToShow.splice(i, 1)
      this.complete.emit({ value: this.clausulasToSend, type: 'CLAUSULAS_ESPECIAIS' })
      this.showClausulaOnResume.emit(this.clausulasToShow)
    }
  }


}
