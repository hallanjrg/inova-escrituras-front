import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public infoService: InfoService,
    private loadingService: LoadingService,
    public dialogRef: MatDialogRef<ConfirmDialogComponent>) { }

  ngOnInit(): void {
    this.infoService.clicked = false
  }

  submit() {
    this.loadingService.isActive = true
    this.infoService.gerarSolicitacao(this.data)
    this.dialogRef.close();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}
