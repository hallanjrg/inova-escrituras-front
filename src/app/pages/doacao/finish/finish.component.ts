import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { InfoService } from 'src/app/services/info.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-finish',
  templateUrl: './finish.component.html',
  styleUrls: ['./finish.component.scss']
})
export class FinishComponent implements OnInit {

  data: any
  codigo: string | null | undefined
  date = moment(new Date()).format('DD/MM/YYYY')

  constructor(private route: Router,
    private activatedRoute: ActivatedRoute,
    public infoService: InfoService,
    public loadingService: LoadingService) { }

  ngOnInit(): void {
    this.data = this.activatedRoute.snapshot.queryParams
  }

  sendHome() {
    this.route.navigate([`/ESCRITURA_DOACAO/${this.activatedRoute.snapshot.params['id']}`],
      {
        queryParams:
        {
          codigoInterno: this.data.codigoInterno,
          codigoCartorio: this.data.codigoCartorio,
          codigoSimples: this.data.codigoSimples
        }
      })
  }

  redirect() {
      this.route.navigate([`/solicitacao-simples/consulta/${this.activatedRoute.snapshot.params['id']}`], {
        queryParams: {
          codigo: this.activatedRoute.snapshot.queryParams.codigoSimples,
          email: this.activatedRoute.snapshot.queryParams.email,
        },
      });
    }

}
