import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { PagesComponent } from './pages.component';
import { RouterModule } from '@angular/router';
import { SolicitacaoSimplesComponent } from './solicitacao-simples/solicitacao-simples.component';
import { AuthGuardService } from '../services/auth-guard';
import { HeaderComponent } from '../components/header/header.component';
import { MenuComponent } from '../components/menu/menu.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import ptBr from '@angular/common/locales/pt';

registerLocaleData(ptBr)


@NgModule({
  declarations: [
    PagesComponent,
    HeaderComponent,
    MenuComponent
  ],
  imports: [
    CommonModule,
    NgbModule,
    /* Rotas de usuário logado - APENAS */
    RouterModule.forChild([
      {
        path: 'solicitacao-simples',
        children: [
          {
            path: 'consulta',
            loadChildren: () => import('./solicitacao-simples/consulta/consulta.module').then(m => m.ConsultaModule),
          },
          {
            path: ':id',
            loadChildren: () => import('./solicitacao-simples/solicitacao-simples.module').then(m => m.SolicitacaoSimplesModule),
          },
          {
            path: 'finish/:id',
            loadChildren: () => import('./solicitacao-simples/finish/finish.module').then(m => m.FinishModule),
          }
        ]
      },
      {
        path: 'solicitacao-completa',
        children: [
          {
            path: '',
            loadChildren: () => import('./solicitacao-completa/solicitacao-completa.module').then(m => m.SolicitacaoCompletaModule),
          }
        ]
      },
      {
        path: 'ESCRITURA_COMPRA_VENDA',
        children: [
          {
            path: ':id',
            loadChildren: () => import('./escrituras/formulario/formulario.module').then(m => m.FormularioModule),
          },
          {
            path: 'finish/:id',
            loadChildren: () => import('./escrituras/finish/finish.module').then(m => m.FinishModule)
          }
        ]
      },
      {
        path: 'ESCRITURA_DOACAO',
        children: [
          {
            path: ':id',
            loadChildren: () => import('./doacao/formulario/formulario.module').then(m => m.FormularioModule),
          },
          {
            path: 'finish/:id',
            loadChildren: () => import('./doacao/finish/finish.module').then(m => m.FinishModule)
          }
        ]
      },
      {
        path: 'fale-conosco',
        children: [
          {
            path: ':id',
            loadChildren: () => import('./fale-conosco/fale-conosco.module').then(m => m.FaleConoscoModule),
          },
          {
            path: 'finish/:id',
            loadChildren: () => import('./fale-conosco/finish/finish.module').then(m => m.FinishModule),
          }
        ]
      },
      {
        path: 'escrituras/consulta/:id',
        /* canActivate: [AuthGuardService], */
        component: SolicitacaoSimplesComponent
      },
      {
        path: 'doacao/consulta/:id',
        /* canActivate: [AuthGuardService], */
        component: SolicitacaoSimplesComponent
      },
      { path: 'registro-imoveis', loadChildren: () => import('./registro-imoveis/registro-imoveis.module').then(m => m.RegistroImoveisModule) }
    ]),
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: "pt-BR"
    }
  ]
})
export class PagesModule { }
