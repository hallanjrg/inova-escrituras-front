import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecuperarsenhaComponent } from './recuperarsenha.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [RecuperarsenhaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {path: '', component: RecuperarsenhaComponent}
    ])
  ]
})
export class RecuperarsenhaModule { }
