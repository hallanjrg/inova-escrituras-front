import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleCompraVendaSolicitation } from 'src/app/models/dataToSend';

@Component({
  selector: 'app-simple-form',
  templateUrl: './simple-form.component.html',
  styleUrls: ['./simple-form.component.scss']
})
export class SimpleFormComponent implements OnInit {

  public simpleSol = false
  public simpleSolData: any = null

  constructor(public modal: NgbActiveModal,
    private fb: FormBuilder) {
    this.simpleSolData = this.fb.group({
      ...new SimpleCompraVendaSolicitation()
    })
  }

  ngOnInit(): void {

  }

}
