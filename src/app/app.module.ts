import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoadingComponent } from './loading/loading.component';
import { SimpleFormComponent } from './pages/escrituras/simple-form/simple-form.component';
import { AuthGuardService } from './services/auth-guard';
import { InterceptorService } from './services/interceptor.service';
import { ToastService } from './services/toast.service';
import { ToastsContainer } from './services/toast/toast-container.component';


@NgModule({
  declarations: [
    AppComponent,
    LoadingComponent,
    SimpleFormComponent,
    ToastsContainer
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgbTooltipModule,
    FormsModule,
    NgbModule,
    NgxMaskModule.forRoot({
      validation: true,
      showMaskTyped: true
    } as Partial<IConfig>),
    ReactiveFormsModule
  ],
  providers: [AuthGuardService, {
    provide: HTTP_INTERCEPTORS,
    useClass: InterceptorService,
    multi: true
  },
  ToastService,
],
  bootstrap: [AppComponent]
})
export class AppModule { }
