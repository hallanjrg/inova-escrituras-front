import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import validator from 'validar-telefone';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  public error = {
    phone: {
      message: ''
    },
    email: {
      message: ''
    }
  }

  constructor() { }

  phoneNumberIsReal(data: string, form: FormGroup) {
    if (data && data.length >= 10) {
      if (!validator(data)) {
        this.error.phone.message = 'O telefone digitado não é válido.'
        form.controls.telefoneSolicitante.setErrors({ 'invalid': true })
        form.controls.telefoneSolicitante.markAsTouched()
      } else {
        this.error.phone.message = ''
      }
    } else {
      this.error.phone.message = 'O campo telefone é obrigatório.'
    }
  }

  validacaoEmail(field: string, form: FormGroup) {
    if (field) {
      let usuario = field.substring(0, field.indexOf("@"));
      let dominio = field.substring(field.indexOf("@") + 1, field.length);
      if ((usuario.length >= 1) &&
        (dominio.length >= 3) &&
        (usuario.search("@") == -1) &&
        (dominio.search("@") == -1) &&
        (usuario.search(" ") == -1) &&
        (dominio.search(" ") == -1) &&
        (dominio.search(".") != -1) &&
        (dominio.indexOf(".") >= 1) &&
        (dominio.lastIndexOf(".") < dominio.length - 1)) {
        this.error.email.message = ''
        form.controls.emailSolicitante.setErrors(null)
      } else {
        this.error.email.message = 'O e-mail digitado não é válido.'
        form.controls.emailSolicitante.setErrors({ 'invalid': true })
      }
    } else {
      this.error.email.message = 'O campo e-mail é obrigatório.'
    }
  }
}
