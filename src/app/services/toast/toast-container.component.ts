import { Component, TemplateRef } from '@angular/core';

import { ToastService } from './toast-service';


@Component({
  selector: 'app-toasts',
  styleUrls: ['./toast.scss'],
  templateUrl: './toast-container.component.html'
})
export class ToastsContainer {
  constructor(public toastService: ToastService) { }

  isTemplate(toast) {
    setTimeout(() => {
      this.closeClick(toast)
    }, 5000);
    return toast.textOrTpl instanceof TemplateRef;
  }

  closeClick(toast) {
    this.toastService.remove(toast);
  }
}

