import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AppGuardService {

  constructor(private router: Router, private api: ApiService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    let i = window.location.pathname.lastIndexOf('/')
    let url = window.location.pathname.substr(i)
    let codigoCartorio = url.replace("/", "");
    if (localStorage.getItem('tkn_esc_ext')) {
      return true;
    } else {
      return this.api.getApiToken(`/sys/token`, codigoCartorio).pipe(
        mergeMap((retorno: any) => {
          localStorage.setItem('tkn_esc_ext', retorno.token);
          return of(true)
        }),
        catchError(() => {
          console.log('erro');
          
          this.router.navigate(['/404'])
          throw Error
        })
      )
    }
  }
}
