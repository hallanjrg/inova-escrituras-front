import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { forkJoin, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient,
    private router: ActivatedRoute) {
  }

  logIn(body: { email: string, password: string }) {
    return this.http.post<{ msg: string, dateHourLogout: any, token: string }>(`${environment.url}/auth/login`, body);
  }

  getApi<T>(params: string): Observable<T> {
    return this.http.get<T>(environment.url + '/secure' + params, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('tkn_esc_ext')}`)
    })
  }

  postApi(params: string, body: any) {
    return this.http.post(environment.url + '/secure' + params, body, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('tkn_esc_ext')}`)
    })
  }


  postApiUpload(params: string, body) {
    return this.http.post(environment.url + params, body)
  }

  /* forkJoin */
  postForkjoin(param1: any, param2: any, param3: any) {
    return forkJoin([param1, param2, param3]).toPromise()
  }


  putApi(url: string, body: any) {
    return this.http.put<string>(environment.url + '/secure' + url, body, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('tkn_esc_ext')}`)
    })
  }

  getApiToken(params: string, codigoCartorio: string) {
    /* let clean = this.router.snapshot.queryParams['codigoCartorio'] */
    return this.http.get(environment.url + params, {
      headers: new HttpHeaders().set('codigoCartorio', codigoCartorio),
    })
  }

  /* no authorization */

  get(params: string) {
    return this.http.get(environment.url + params)
  }

  post(params: string, body: any) {
    return this.http.post(environment.url + params, body, {
      headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem('tkn_esc_ext')}`)
    })
  }

  /* cep */

  getCep(cep) {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`)
  }

  getApiListaEstados(params) {
    return this.http.get(environment.url + params)
  }

}



