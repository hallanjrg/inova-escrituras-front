import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';
import { ApiService } from './api.service';
import { LoadingService } from './loading.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  public requests: any = []
  constructor(private loadingService: LoadingService, private api: ApiService, private route: ActivatedRoute) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loadingService.isActive = true;
    return next.handle(req).pipe(
      catchError(error => {
        this.removeRequest()
        throw error
      })
    ).pipe(
      finalize(() => {
        this.removeRequest()
      }))
  }

  removeRequest() {
    this.requests.splice(0, 1)
    if (this.requests.length == 0) {
      this.loadingService.isActive = false
    }
  }
}
