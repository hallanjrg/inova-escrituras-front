import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { LoadingService } from './loading.service';
import { Router } from '@angular/router';
import { Uploads } from '../models/uploads';
import { ISolicitacao } from '../models/solicitacoes';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InfoService {

  /* Arquivos */
  uploads: Uploads = {
    documentosBem: [],
    documentosComprador: [],
    documentosVendedor: []
  }
  public mockFile: any = []
  /* Arquivos */

  /* Dados da solicitação */
  resultadoPesquisa: any
  showRes = false
  showPendingItems: any
  showChatPendinfItems: any
  /* Dados da solicitação */

  clicked = false
  public atualizarTela = new BehaviorSubject<boolean>(false)

  constructor(private apiService: ApiService,
    /* public loadingService: LoadingService, */
    public router: Router
  ) { }

  consultar(codigo: string, email: string, type: string) {
    this.showRes = false
    this.resultadoPesquisa = null
    /* this.loadingService.isActive = true */
    this.apiService.getApi(`/solicitacao/get?codigo=${codigo}&tipoEscritura=${type}`).subscribe((res: any) => {
     
      this.getPendingItems(res.codigoInterno)
      this.getChatPendingItems(res.codigoInterno)
      this.showRes = true
      this.resultadoPesquisa = res
    }, err => {
      this.showRes = false
      this.resultadoPesquisa = null
      /* this.loadingService.isActive = false */
    })
  }

  getPendingItems(codigo: string) {
   /*  this.loadingService.isActive = true */
    this.apiService.getApi(`/pendencia/listar?codigoSolicitacao=${codigo}`).subscribe((res: any) => {
      this.showPendingItems = res
      /* this.loadingService.isActive = false */
    }, err => {
      /* this.loadingService.isActive = false */
      this.showRes = true
    })
  }

  getChatPendingItems(codigo: string) {
    /* this.loadingService.isActive = true */
    this.apiService.getApi(`/chat/listar?codigoSolicitacao=${codigo}`).subscribe((res: any) => {
      this.showChatPendinfItems = res
      /* this.loadingService.isActive = false */
    }, err => {
      /* this.loadingService.isActive = false */
      this.showRes = true
    })
  }


  gerarSolicitacao(formulario: any) {
    /* this.loadingService.isActive = true */
    let formData = new FormData
    this.uploads.documentosBem.forEach(element => {
      formData.append('documentosBem', element)
    })
    this.uploads.documentosComprador.forEach(element => {
      formData.append('documentosComprador', element)
    })
    this.uploads.documentosVendedor.forEach(element => {
      formData.append('documentosVendedor', element)
    })
    formData.append('nomeContato', formulario.nomeContato)
    formData.append('telefoneContato', formulario.telefoneContato)
    formData.append('emailContato', formulario.emailContato)
    formData.append('valorBem', formulario.valorBem)
    formData.append('codigoCartorio', formulario.codigoCartorio)
    formData.append('qtdCompradores', formulario.qtdCompradores)
    formData.append('qtdVendedores', formulario.qtdVendedores)

    this.apiService.postApi('/solicitacao/save', formData).subscribe(res => {
      this.router.navigate(['/finish/' + res])
    }, err => {
      /* this.loadingService.isActive = false */
    })
  }



}
