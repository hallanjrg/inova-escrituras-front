import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { timeout, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  estados = [];
  municipios;

  constructor(private apiService: ApiService, private http: HttpClient, private login: LoginService) { }

  getFreight(zipCode) {
    if (zipCode.value.length === 8) return this.calcFreight(zipCode.value);
  }


  async calcFreight(zipCode: string) {
    return this.http.
      get(`${environment.url}/secure/correios/calcularfrete?origem=${this.login.loggedUser.cep}&destino=${zipCode}`, {
        headers: new HttpHeaders().append('Authorization', 'Bearer ' + localStorage.getItem('tkn_esc_ext'))
      })
      .pipe(timeout(10000), catchError(e => { return of(e) })).toPromise()
  }

  getEstados() { // novo
    this.apiService.getApiListaEstados(`/dominio/estados`).subscribe(res => {
      this.estados = Object.entries(res)
      let newEstados = []
      this.estados.forEach(element => {
        newEstados.push(element[0])
      });
      this.estados = newEstados.sort()
    })
  }

  getMunicipiosPorEstado(uf) { //novo
    this.apiService.getApiListaEstados(`/dominio/municipios?uf=${uf}`).subscribe(res => {
      this.municipios = res
    })
  }
}
