import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    const { codigo, email } = route.queryParams
    this.router.navigate([`solicitacao-simples/consulta/${route.params['id']}`], {
      queryParams: {
        codigo,
        email
      }
    })
    return true
  }

}
