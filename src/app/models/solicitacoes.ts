export interface ISolicitacao{
    codigo: number,
    codigoCartorio: string,
    dataCriacao: Date,
    documentosBem: [],
    documentosComprador: [],
    documentosVendedor: [],
    emailContato: string,
    nomeContato: string,
    telefoneContao: string,
    valorBem: number
}