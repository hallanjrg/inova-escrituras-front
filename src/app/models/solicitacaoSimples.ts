export class SolicitacaoSimples {
    canal: string = '2';
    nomeSolicitante: any = null;
    emailSolicitante: any = null;
    telefoneSolicitante: any = null;
    codigoResponsavel: any = null;
    tipoEscritura: any = null;
    codigoStatus: number = 1;
    ignorarValidacao: boolean = true;
    mensagem: any = '';
    arquivo: any = null;

    static new(tipo?: string) {
        let n = new SolicitacaoSimples()
        if (tipo) {
            n.tipoEscritura = tipo
        }
        return n
    }
}

export class Solicitation {
    solicitor: Solicitor = new Solicitor();
    clausulasEscritura: Array<any> = [];
    questoesEscritura: Array<any> = [];
    infoSolicitation: InfoSolicitation = new InfoSolicitation();
    bens: Array<Asset> = [new Asset()];
    compradores: Array<Concerned> = [new Concerned()];
    vendedores: Array<Concerned> = [new Concerned()];
    doadores: Array<Concerned> = [new Concerned()];
    beneficiarios: Array<Concerned> = [new Concerned()];
    envolvidos: Array<Concerned | Asset> = [];
    tipoEscritura: string = '';
    static new(data?: any) {
        const s = new Solicitation();
        if (data) {
            s.infoSolicitation.codigo = data.codigo
            s.infoSolicitation.codigoCartorio = data.codigoCartorio
            s.infoSolicitation.codigoInterno = data.codigoInterno
            s.infoSolicitation.codigoStatus = data.codigoStatus
            s.infoSolicitation.dataAgendamento = data.dataAgendamento
            s.infoSolicitation.dataCriacao = data.dataCriacao
            s.infoSolicitation.mensagem = data.mensagem
            s.infoSolicitation.motivoCancelamento = data.motivoCancelamento
            s.infoSolicitation.pausado = data.pausado
            s.infoSolicitation.status = data.status
            s.infoSolicitation.tipoEscritura = data.tipoEscritura
            s.clausulasEscritura = data.clausulasEscritura
            s.questoesEscritura = data.questoesEscritura
            s.solicitor.emailSolicitante = data.emailSolicitante
            s.solicitor.identificacao = data.identificacao
            s.solicitor.telefoneSolicitante = data.telefoneSolicitante
            s.solicitor.nomeSolicitante = data.nomeSolicitante
            s.bens = data.bens
            s.compradores = data.compradores
            s.vendedores = data.vendedores
            s.beneficiarios = data.beneficiarios
            s.doadores = data.doadores
        }
        return s
    }
}

export class Solicitor {
    nomeSolicitante: string = '';
    telefoneSolicitante: string = '';
    emailSolicitante: string = '';
    identificacao?: string = '1';
    mensagem?: string = '';
}

export class InfoSolicitation {
    aguardandoCliente: boolean = false;
    canal: number = 0;
    codigo: number = 0;
    codigoCartorio: string = '';
    codigoInterno: string = '';
    codigoStatus: number = 0;
    dataAgendamento: any = null;
    dataCriacao: string = '';
    mensagem: string = '';
    motivoCancelamento: string = '';
    msg: any
    pausado: boolean = false;
    status: string = '';
    tipoEscritura: string = '';
}

export class Asset {
    codigo: number = 0;
    documentos: Array<Document> = [new Document()]
    tipo: string = '';
    tipoImovel: number = 0;
    valor: number = 0;
}

export class Concerned {
    codigo: number = 0;
    documentos: Array<Document> = [new Document()]
    compartilharInfo: boolean = false;
    email: string = '';
    nome: string = '';
    telefone: string = '';
    tipoPessoa: string = '';
    tipoEnvolvido: string = ''
    static new(c: string) {
        let ret = new Concerned()
        if (c) {
            ret.tipoEnvolvido = c
        }
        return ret
    }
}


export class Document {
    codigo: number = 0;
    nome: string = '';
    link: string = '';
    obs: string = '';
    valido: boolean = false;
}
