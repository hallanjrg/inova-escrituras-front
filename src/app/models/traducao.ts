export const TRADUCAO_CERTIDAO = {
    DOMINAL: {
        "chave": "DOMINAL",
        "descricao": "Dominal"
    },
    BREVE_RELATO: {
        "chave": "BREVE_RELATO",
        "descricao": "Breve relato"
    },
    MATRICULA_LIVRO_2: {
        "chave": "MATRICULA_LIVRO_2",
        "descricao": "Matricula do Livro 2 - Inteiro Teor"
    },
    MATRICULA_LIVRO_3: {
        "chave": "MATRICULA_LIVRO_3",
        "descricao": "Matricula do Livro 3 - Inteiro Teor"
    },
    TRANSCRICAO: {
        "chave": "TRANSCRICAO",
        "descricao": "Transcrição"
    },
    ONUS_REAIS: {
        "chave": "ONUS_REAIS",
        "descricao": "Ônus reais"
    },
    PEDIDO_BUSCA_BENS: {
        "chave": "PEDIDO_BUSCA_BENS",
        "descricao": "Pedido de busca de bens"
    },
    ACOES_REAIS_REIPERSECUTORIAS: {
        "chave": "ACOES_REAIS_REIPERSECUTORIAS",
        "descricao": "Ações reais reipersecutórias"
    },
    DOCUMENTO_ARQUIVADO_CONHECIMENTO: {
        "chave": "DOCUMENTO_ARQUIVADO_CONHECIMENTO",
        "descricao": "Documento Arquivado e Conhecimento"
    },
    PENHOR: {
        "chave": "PENHOR",
        "descricao": "Penhor"
    }
}