export const ESCRITURA_DOACAO = {
    1: {
        value: 1,
        description: 'Bem',
        plural: 'Bens'
    },
    2: {
        value: 2,
        description: 'Doador',
        plural: 'Doadores'
    },
    3: {
        value: 3,
        description: 'Beneficiário',
        plural: 'Beneficiários'
    },
}

export const ESCRITURA_COMPRA_VENDA = {
    1: {
        value: 1,
        description: 'Bem',
        plural: 'Bens'
    },
    2: {
        value: 2,
        description: 'Comprador',
        plural: 'Compradores'
    },
    3: {
        value: 3,
        description: 'Vendedor',
        plural: 'Vendedores'
    },
}