export interface Message {
    dataCriacao: Date;
    mensagem: string;
    linkArquivo: string;
    codigoSolicitacao: string;
    remetente: string;
    msgSolicitante: boolean;

}
export class FileType {
    codigoArquivo: string = '';
    nome: string = '';
}
