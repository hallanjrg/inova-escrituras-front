import { ClausulasEspeciais } from "./clausulas-especiais";
import { Questionario } from "./questionario";

export interface ConfirmDoacao {
  canal: string,
  identificacao: any,
  codigoCartorio: string,
  nomeSolicitante: string,
  telefoneSolicitante: string,
  emailSolicitante: string,
  qtd?: qtd,
  doadores: Array<Object>,
  beneficiarios: Array<Object>,
  bens: Array<Object>,
  questoesEscritura: Array<Questionario>,
  clausulasEscritura: Array<ClausulasEspeciais>,
  tipoEscritura: any,
  msg?: any,
  codigo?: any,
  codigoInterno?: any
  codigoStatus?: any
}

interface qtd {
  qtdBens: string,
  qtdDoadores: string,
  qtdBeneficiario: string
}

