export interface Questionario {
    codigo: number;
    descricao?: string;
    resposta: any;
    tipo: number;
}