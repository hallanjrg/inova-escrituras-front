export interface Uploads {
    documentosBem: Array<any>,
    documentosComprador: Array<any>,
    documentosVendedor: Array<any>
}

export class FileType {
  nome: string = '';
}
