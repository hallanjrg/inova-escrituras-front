export class FaleConosco {
  canal: string = '2';
  nome: any = null;
  email: any = null;
  telefone: any = null;
  assunto: any = '';
  msg: any = '';
  arquivo: any =null;
  static new(tipo?: string) {
    let n = new FaleConosco()
    if (tipo) {
        n.assunto = tipo
    }
    return n
}

}
