export class BuyerData {
    tipoPessoa: string = '';
    nome: string = '';
    email: string = '';
    telefone: string = '';
    documentos: Array<DocumentType> = [];
    items?: Array<DocumentType> = [];
    compartilharInfo: boolean = false

    static vazio() {
        let n = new BuyerData()
        n.email = 'email@solicitante'
        n.nome = 'nome solicitante'
        n.telefone = 'telefone solicitante'
        n.tipoPessoa = 'FISICA'
        delete n.items
        return [n]
    }
}

class DocumentType {
    id: string = '';
    nome: string = '';
    descricao: string = '';
}