import { AssetData } from "./assetData";
import { BuyerData } from "./buyerData";
import { SellerData } from "./sellerData";

export interface Confirm {
  codigo?: string,
  codigoInterno?: string,
  canal: string,
  identificacao: any,
  codigoCartorio: string,
  nomeSolicitante: string,
  telefoneSolicitante: string,
  emailSolicitante: string,
  qtd?: qtd,
  compradores: Array<Object>,
  vendedores: Array<Object>,
  bens: Array<Object>,
  tipoEscritura: any,
  msg: string,
  codigoStatus: number
}

interface qtd {
  qtdBens: string,
  qtdCompradores: string,
  qtdVendedores: string
}

export class SimpleCompraVendaSolicitation {
  canal: string = '2';
  identificacao: any = 6;
  codigoCartorio: any = '';
  nomeSolicitante: string = '';
  telefoneSolicitante: string = '';
  emailSolicitante: string = '';

  compradores: Array<Object> = [BuyerData.vazio()];
  vendedores: Array<Object> = [SellerData.vazio()];
  bens: Array<Object> = [AssetData.vazio()];
  tipoEscritura: any = 'ESCRITURA_COMPRA_VENDA';
}


