export class AssetData {
    tipoCodigo: number | undefined;
    tipoBem?: any = null;
    listaTipoBem?: Array<any> = []
    tipos?: Array<any> = [];
    valor: number | null = null;
    documentos: Array<DocumentType> = [];
    items?: Array<DocumentType> = []
    static vazio() {
        let n = new AssetData()
        n.tipoBem = "BEM-RURAL"
        n.tipoCodigo = 4
        n.valor = 0
        delete n.tipos
        delete n.listaTipoBem
        delete n.items

        return [{ ...n, tipoImovel: 5 }]
    }
}

class DocumentType {
    id: string = '';
    nome: string = '';
    descricao: string = '';
}