export const TYPOLOGY = {
    'ESCRITURA_COMPRA_VENDA': {
        concerneds: [
            {
                description: 'Bem',
                plural: 'Bens'
            },
            {
                description: 'Comprador',
                plural: 'Compradores',
            },
            {
                description: 'Vendedor',
                plural: 'Vendedores',
            }
        ]
    },
    'ESCRITURA_DOACAO': {
        envolvidos: [
            {
                description: 'Doador',
                plural: 'Doadores',
            },
            {
                description: 'Beneficiário',
                plural: 'Beneficiários',
            }
        ],
        informacaoComplementar: [
            {
                description: 'Questionário',

            }
        ]
    }
}