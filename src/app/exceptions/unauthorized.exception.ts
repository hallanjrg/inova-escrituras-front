export class UnauthorizedException extends Error {
  constructor() {
      super('Usuário e senha invalidos');
  }
}
export class InvalidParameters extends Error {
  constructor() {
      super('Parâmetros inválidos');
  }
}
export class InvalidUser extends Error {
  constructor() {
      super('O e-mail informado não é valido');
  }
}
